
# Project setup

I followed this tutorial: https://tutorialedge.net/typescript/typescript-socket-io-tutorial/

postgresql - requires two environment variables:
    DEPLOYMENT = 'LIVE'
    DATABASE_URL = '...'

If the DEPLOYMENT variable does not exist, then it will connect to localhost postgres with a hardcoded username/password in dbService.ts (useful for local testing).

## Things to install:

`npm install -g nodemon`
`npm install -g yarn`

## Start the project:

`npm run watch`

`ngrok http 3000`


# Heroku Deployment

Push to heroku `main` branch to automatically deploy that code to Heroku.

`git push heroku master`

Follow heroku logs:

`heroku logs --tail`

Connect to heroku psql

`heroku pg:psql postgresql-round-63957 --app stack-mate`

# Database Migrations (DB Breaking changes)



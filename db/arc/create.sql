
CREATE TABLE account (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    name_lower VARCHAR(20) NOT NULL,
    password VARCHAR(76),
    coins INTEGER DEFAULT 0,
    created_at TIMESTAMP DEFAULT NOW(),
    active BOOLEAN DEFAULT FALSE
);

CREATE TABLE game_mode (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) UNIQUE NOT NULL,
    dsc VARCHAR(255) NOT NULL,
    json TEXT
);

CREATE TABLE rating (
    id SERIAL PRIMARY KEY,
    game_mode INTEGER REFERENCES game_mode(id),
    season INTEGER,
    account INTEGER REFERENCES account(id),
    rating DOUBLE PRECISION NOT NULL,
    rd DOUBLE PRECISION NOT NULL,
    vol DOUBLE PRECISION NOT NULL,
    matches INTEGER,
    UNIQUE (game_mode, season, account)
);

CREATE TABLE preset (
    position INTEGER PRIMARY KEY,
    game_mode INTEGER NOT NULL REFERENCES game_mode(id)
);

CREATE TABLE server_room (
    position INTEGER PRIMARY KEY,
    game_mode INTEGER NOT NULL REFERENCES game_mode(id),
    season INTEGER NOT NULL
);

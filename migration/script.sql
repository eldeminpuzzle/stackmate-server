
ALTER TABLE preset RENAME COLUMN game_mode TO game_mode_id;
ALTER TABLE rating RENAME COLUMN game_mode TO game_mode_id;
ALTER TABLE rating RENAME COLUMN account TO account_id;
ALTER TABLE server_room RENAME COLUMN game_mode TO game_mode_id;


ALTER TABLE account RENAME COLUMN name_lower TO nameLower;
ALTER TABLE preset RENAME COLUMN game_mode_id TO gameModeId;
ALTER TABLE rating RENAME COLUMN game_mode_id TO gameModeId;
ALTER TABLE rating RENAME COLUMN account_id TO accountId;
ALTER TABLE server_room RENAME COLUMN game_mode_id TO gameModeId;

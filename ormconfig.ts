import { SnakeNamingStrategy } from "typeorm-naming-strategies";

module.exports = {
  "url": process.env.DATABASE_URL,
   "type": "postgres",
  //  "host": "localhost",
  //  "port": 5432,
  //  "username": "postgres",
  //  "password": "sepatu",
  //  "database": "tbs_local",

   // turn both of those to true to reset DB (lose all data)
   "dropSchema": false,
   "synchronize": true,

   "logging": false,
   "entities": [
      "src/entity/**/*.ts"
   ],
   "migrations": [
      "src/migration/**/*.ts"
   ],
   "subscribers": [
      "src/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   },
   "ssl": true,
   "extra": {
     "ssl": process.env.DATABASE_IGNORE_SSL === 'true' ? false : { rejectUnauthorized: false }
    //  "ssl": false, // process.env.DATABASE_IGNORE_SSL === 'true' ? false : { rejectUnauthorized: false }
   },

   namingStrategy: new SnakeNamingStrategy()
}
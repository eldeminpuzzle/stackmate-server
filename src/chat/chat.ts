import { Client } from "../client";
import { Context } from "../context";
import { Message } from "../shared/chat/message";

export class ChatService {

    private buffer: Message[] = [];
    private bufferLimit = 40;

    private entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };

    addMessage(client: Client, message: string) {
        if (this.buffer.length > this.bufferLimit) {
            this.buffer.shift();
        }
        
        let msg: Message = {
            message: message,
            timestamp: Date.now(),
            username: client.username,
        };
        this.buffer.push(msg);

        Context.get().getDiscordBot().sendMessage(msg);
        return msg;
    }

    getMessages() {
        return this.buffer;
    }
}

import e = require("express");
import { Context } from "./context";
import { DataUtils } from "./dataUtils";
import { Room } from "./room/room";
import { SideData } from "./shared/gameData";
import { GameRule } from "./shared/gameRule";
import { RoomEnteredInfo, PlayerJoinedInfo, AccountInfo } from "./shared/intf";
import { MoveInfo, SubmoveInfo } from "./shared/playerEngine";
import { SocketWrapper } from "./shared/socketWrapper";
import { Util } from "./shared/util";
import { LoginData } from "./db/account";

/**
 * A Client instance is created for every connected client (socket).
 * 
 * This class contains:
 * - information about the client (login, id)
 * - socket event handlers
 * 
 * 
 * TODOs:
 *      - create/join room not allowed if the client is already in a room
 *      - authentication (create/join room not allowed if not logged in)
 */

export class Client {
    static nextId = 0;

    id: number;
    accountId: number;
    username: string;
    roomId: number;

    // DI?
    ctx: Context = Context.get();
    presetsLoaded = false;

    randomNames = ['Albatross','Alligator','Anteater','Antelope','Ape','Armadillo','Donkey','Baboon','Badger','Barracuda','Bat','Bear','Beaver','Bee','Bison','Bluebird','Boar','Buffalo','Butterfly','Camel','Capybara','Caribou','Cassowary','Cat','Caterpillar','Cheetah','Chicken','Chimpanzee','Chinchilla','Cobra','Cougar','Coyote','Crab','Cricket','Crocodile','Crow','Deer','Dog','Dolphin','Donkey','Dragonfly','Duck','Dugong','Eagle','Echidna','Eel','Elephant','Elk','Falcon','Flamingo','Fox','Frog','Gazelle','Gecko','Giraffe','Goat','Goose','Gorilla','Grasshopper','Hamster','Hawk','Hedgehog','Hippopotamus','Hornet','Horse','Hyena','Iguana','Jaguar','Jellyfish','Kangaroo','Koala','Komodo','Lemur','Leopard','Lion','Lizard','Lynx','Mammoth','Meerkat','Mole','Mongoose','Mouse','Nightingale','Ocelot','Octopus','Orangutan','Ostrich','Otter','Ox','Owl','Oyster','Panther','Parrot','Panda','Penguin','Rabbit','Raccoon','Raven','Reindeer','Rhinoceros','Salamander','Seahorse','Seal','Shark','Snake','Spider','Squirrel','Swan','Tiger','Toucan','Viper','Weasel','Wolf','Zebra'].map(s => s.toUpperCase());

    constructor(private socket: SocketWrapper)
    {
        this.id = Client.nextId++;
        this.log('connected');

        this.ctx.getLobbyService().addClient(this);
        
        Util.autoBind(this, socket);
    }

    emit(eventName: string, ...data: any) {
        this.socket.send(eventName, ...data);
    }

    getUsername() { return this.username; }

    forceDisconnect() {
        this.socket.forceDisconnect();
    }

    on_message(message: string) {
        let msg = this.ctx.getChatService().addMessage(this, message);
        this.ctx.getLobbyService().broadcastToAllClients('message', msg);
    }



    on_disconnect(e: any) {
        this.log('disconnected');

        if (this.roomId != null) {
            this.ctx.getLobbyService().exitRoom(this.roomId, this);
        }
        
        this.ctx.getLobbyService().removeClient(this);
    }

    err(reason: any) {
        console.error(reason);
        console.trace();
    }


    // ----------------------- Login / Register -----------------------
    on_checkAccountExists(username: string) {
        this.ctx.getAccountService().getLoginData(username).then(v => {
            this.emit('checkAccountExists', username, v != null);
        }).catch(this.err);
    }

    on_login(username: string, clearTextPassword: string) {
        let a = this.ctx.getAccountService();
        a.getLoginData(username).then(loginData => {
            if (loginData == null) {
                // login as anonymous
                this.accountId = null;
                this.username = username;
                this.handleLogin({accountId: null, username, coins: 0, password: null});
                Context.get().getDiscordBot().sendSecret(this.username + ' logged in!');
            } else {
                // login as registered user
                a.checkPassword(clearTextPassword, loginData.password).then(match => {
                    if (match) {
                        this.handleLogin(loginData);
                        Context.get().getDiscordBot().sendSecret(this.username + ' logged in!');
                    } else {
                        this.emit('wrongPassword', this.username);
                    }
                }).catch(this.err);
            }
        }).catch(this.err);
    }

    private async handleLogin(loginData: LoginData) {
        if (this.roomId != null) {
            Context.get().getLobbyService().exitRoom(this.roomId, this);
        }

        this.accountId = loginData.accountId;
        this.username = loginData.username;

        let accountInfo: AccountInfo = {
            name: this.username,
            anonymous: this.accountId == null,
            coins: loginData.coins,
        }
        this.emit('login', accountInfo);

        if (!this.presetsLoaded) {
            this.presetsLoaded = true;
            this.emit('presetInfos', Context.get().getGameRuleService().getPresets());
        }
    }
    
    on_register(username: string, clearTextPassword: string) {
        if (this.randomNames.indexOf(username) != -1) {
            throw new Error('Attempted to register a reserved name!');
        }
        
        if (username.match(/^[a-zA-Z0-9]+$/) && clearTextPassword.length > 0) {
            this.ctx.getAccountService().getLoginData(username).then(v => {
                if (v == null) {
                    this.ctx.getAccountService().register(username, clearTextPassword).then(success => {
                        if (success) {
                            this.emit('registered', username);
                        } else {
                            this.emit('registrationFailed', username);
                        }
                    }).catch(this.err);
                } else {
                    this.emit('checkAccountExists', username, true);
                }
            }).catch(this.err);
        }
    }

    on_logout() {
        if (this.roomId != null) {
            this.ctx.getLobbyService().exitRoom(this.roomId, this);
            this.roomId = null;
        }
        this.username = null;
    }
    // ----------------------------------------------------------------






    on_createRoom(name: string, presetId: number, custom: boolean) {
        if (this.username == null) {
            this.log('Cannot create room because the client is not logged in.');
            
        } else if (this.roomId != null) {
            this.log('Cannot create room because the user is already in another room.');

        } else {
            let room = this.ctx.getLobbyService().createRoom(name, this, presetId, custom);
            if (room != null) {
                this.roomId = room.getRoomData().id;
                this.emitRoomEntered(room, 0);
            }
        }
    }

    private emitRoomEntered(room: Room, seatNr: number) {
        let packet: RoomEnteredInfo = {
            room: room.getRoomInfo(this),
            seatNr,
            gameRuleJson: room.gameRuleJson
        }
        this.emit('roomEntered', packet);
    }

    on_getRooms() {
        this.emit("updateRoomInfos", this.ctx.getLobbyService().getRoomInfos());
    }

    on_joinRoom(roomId: number, spectateMode: boolean) {
        if (this.roomId == roomId) return; // already in room

        // exit previous room
        if (this.roomId != null) {
            this.ctx.getLobbyService().exitRoom(this.roomId, this);
        }
        
        // on server room, the same player cannot join twice (rated games)
        let room = this.ctx.getLobbyService().getRoom(roomId);
        if (this.accountId != null 
            && room.getRoomData().serverRoom 
            && room.getRoomData().players.find(p => p != null && p.id == this.accountId) != null) {
                console.log('double enter');
                return;
        }
            
        // enter room
        if (room != null) {
            let seatNr = room.enterRoom(this, spectateMode);
            this.roomId = roomId;

            // notify client that he is in
            this.emitRoomEntered(room, seatNr);

            // broadcast to others in room
            let packet: PlayerJoinedInfo = {
                seatNr, player: DataUtils.getPlayerInfo(this)
            };
            room.broadcast('playerJoined', this, packet);
        }
    }

    on_autoJoin() {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.autoJoin(this);
        }
    }

    on_exitRoom() {
        this.ctx.getLobbyService().exitRoom(this.roomId, this);
        this.roomId = null;;
    }


    // room
    on_startGame() {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null && room.isHost(this) && room.isRequiredPlayersReady()) {
            room.start();
        }
    }

    on_updateGameRule(gameRule: GameRule, json: string) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null && room.isHost(this)) {
            room.onUpdateGameRuleReceived(gameRule, json);
        }
    }

    on_switchSeatNr(sideNr: number) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onSwitchSeatNr(this, sideNr);
        }
    }

    on_transferHost(newHost: number, newHostIsSpectator: boolean) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onTransferHost(this, newHost, newHostIsSpectator);
        }
    }

    on_setAutoJoin(autoJoin: boolean) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onSetAutoJoin(autoJoin);
        }
    }

    on_setAutoStart(autoStart: number) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onSetAutoStart(autoStart);
        }
    }


    // game
    on_submitMove(moveInfo: MoveInfo) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        room.onMoveReceived(moveInfo, this);
    }

    on_subMove(subMoveInfo: SubmoveInfo) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onSubMoveReceived(subMoveInfo, this);
        }
    }

    on_die() {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        if (room != null) {
            room.onDieReceived(this);
        }
    }



    // leaderboard
    /* client needs to know which leaderboards are avaiable...
        gameModeId
        gameModeName
        maxSeason (all previous seasons are available too)
    */
    on_loadLeaderboardList() {
        this.emit('loadLeaderboardList', Context.get().getGameRuleService().getLeaderboardList());
    }


    on_loadLeaderboard(gameModeId: number, season: number, offset: number, limit: number) {
        if (limit > 50) {
            console.error('Max limit on loadLeaderboard is 50!');
        }
        
        Context.get().getRatingService().loadLeaderboard(gameModeId, season, offset, limit).then(data => {
            this.emit('loadLeaderboard', data);
        }).catch(this.err);;
    }



    // debug
    on_debugData(sideData: SideData) {
        let room = this.ctx.getLobbyService().getRoom(this.roomId);
        room.onDebugData(this, sideData);
    }

    log(msg: string) {
        console.log('Client ' + this.id + ': ' + msg);
    }
}

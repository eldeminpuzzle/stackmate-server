import { ChatService } from "./chat/chat";
import { AccountService } from "./db/account";
import { DbService } from "./db/dbService";
import { GameModeInfo, GameRuleService } from "./db/gameRuleService";
import { RatingService } from "./db/rating";
import { DiscordBot } from "./discord/discord";
import { LobbyService } from "./room/lobby";
import { PresetInfo } from "./shared/intf";

export class Context {
    private static instance = new Context();
    static get() {
        return Context.instance;
    }

    async init() {
        return this.getGameRuleService().init()
    }

    private lobbyService = new LobbyService();
    private chatService = new ChatService();
    private dbService = new DbService();
    private gameRuleService = new GameRuleService();
    private accountService = new AccountService();
    private ratingService = new RatingService();
    private discordBot = new DiscordBot();

    public getLobbyService() { return this.lobbyService; }
    public getChatService() { return this.chatService; }
    public getDbService() { return this.dbService; }
    public getGameRuleService() { return this.gameRuleService; }
    public getAccountService() { return this.accountService; }
    public getRatingService() { return this.ratingService; }
    public getDiscordBot() { return this.discordBot; }
}

import { Client } from "./client";
import { PlayerInfo } from "./shared/intf";

export class DataUtils {
    static getPlayerInfo(client: Client): PlayerInfo {
        return client == null ? null : { username: client.getUsername()};
    }
}

import * as bcrypt from 'bcrypt';
import { EntityManager } from "typeorm";
import { Account } from "../entity/account";
import { t } from "./dbService";

export interface LoginData {
  accountId: number;
  username: string;
  password: string;
  coins: number;
}

export class AccountService {

  private async loadAccount(em: EntityManager, username: string): Promise<Account> {
    return em.findOne(Account, { nameLower: username.toLowerCase() });
  }

  private async acc(username: string, callback: (em: EntityManager, account: Account) => Promise<any>): Promise<any> {
    return t(async em => {
      let account: Account = await this.loadAccount(em, username);
      return callback(em, account);
    })
  }

  async getLoginData(username: string): Promise<LoginData> {
    return this.acc(username, async (em, account) => {
      if (account == null) {
        return null;
      } else {
        return {
          accountId: account.id,
          username: account.name,
          password: account.password,
          coins: account.coins,
        };
      }
    });
  }

  async register(username: string, clearTextPassword: string): Promise<boolean> {
    return this.acc(username, async (em, account) => {
      if (account == null) {
        const password = await bcrypt.hash(clearTextPassword, 12);

        await em.insert(Account, {
          name: username,
          nameLower: username.toLowerCase(),
          password: password,
        });
        return true;
      } else {
        return false;
      }
    });
  }

  async checkPassword(clearTextPassword: string, password: string): Promise<boolean> {
    return bcrypt.compare(clearTextPassword, password);
  }

  async addCoins(username: string, amount: number) {
    return this.acc(username, async (em, account) => {
      account.coins += amount;
      return em.save(account);
    });
  }

  async getCoins(username: string): Promise<number> {
    return this.acc(username, async (em, account) => {
      return account.coins;
    });
  }
}

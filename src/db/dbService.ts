
import { Pool, PoolClient, QueryResult } from 'pg';
import { EntityManager, getManager } from 'typeorm';
import { Context } from '../context';

export async function t(callback: (em: EntityManager) => Promise<any>): Promise<any> {
    return await getManager().transaction(callback);
}

export class DbService {

    // pool: Pool;

    // constructor() {
    //   console.log('env', process.env.DEPLOYMENT);
    //   console.log('connectionString', process.env.DATABASE_URL);
    //     if (process.env.DEPLOYMENT == 'LIVE') {
    //         this.pool = new Pool({
    //             connectionString: process.env.DATABASE_URL,
    //             ssl: { rejectUnauthorized: false }
    //         });
    //     } else {
    //         this.pool = new Pool({
    //             connectionString: 'postgresql://postgres:sepatu@localhost/tbs_local?sslmode=disable',
    //             ssl: false
    //         });
    //     }
    // }

    // query(queryText: string, values: any[], callback: (result: QueryResult<any>) => void) {
    //     this.pool.query(queryText, values, (err, res) => {
    //         if (err == null) {
    //             callback(res);
    //         } else {
    //             console.error(err);
    //             throw new Error('Error while executing DB query');
    //         }
    //     });
    // }

    // async transaction(callback: (client: PoolClient) => Promise<any>): Promise<any> {
    //     let dbService = Context.get().getDbService();
    //     const client = await dbService.pool.connect();
    //     try {
    //         await client.query('BEGIN');
    //         let ret = await callback(client);
    //         await client.query('COMMIT');
    //         return ret;
    //     } catch (err) {
    //         console.trace(err);
    //         await client.query('ROLLBACK');
    //         return null;
    //     } finally {
    //         client.release();
    //     }
    // }
}

import { LeaderboardListItem, PresetInfo } from "../shared/intf";
import { t } from "./dbService";
import { GameMode } from "../entity/gameMode";
import { Preset } from "../entity/preset";
import { Rating } from "../entity/rating";

export interface GameModeInfo {
    id: number;
    json: string;
    name: string;
}

export class GameRuleService {

    private presetToGameMode: Map<number, GameModeInfo> = null;
    private idToGameMode: Map<number, GameModeInfo> = null;
    private presets: PresetInfo[] = null;
    private leaderboardList: LeaderboardListItem[] = null;

    async init() {
        await this.loadPresetMap();
        await this.loadPresets();
        await this.loadGameModeMap();
        await this.loadLeaderboardList();
    }

    private async loadPresets(): Promise<PresetInfo[]> {
        return t(async em => {
            const presets: Preset[] = await em
                .createQueryBuilder(Preset, 'p')
                .innerJoinAndSelect('p.gameMode', 'gm')
                .orderBy('p.position')
                .getMany();
            
            this.presets = presets.map((p: Preset) => ({
                position: p.position,
                name: p.gameMode.name,
                dsc: p.gameMode.dsc,
            }));
        });
    }

    private async loadPresetMap() {
        return t(async em => {
            const presets: Preset[] = await em
                .createQueryBuilder(Preset, 'p')
                .innerJoinAndSelect('p.gameMode', 'gm')
                .getMany();
            
            let ret = new Map<number, GameModeInfo>();
            presets.forEach(row => {
                ret.set(row.position, this.toGameModeInfo(row.gameMode));
            });
            this.presetToGameMode = ret;
            return true;
        });
    }

    private async loadGameModeMap() {
        return t(async em => {
            const gameModes = await em.find(GameMode);
            
            let ret = new Map<number, GameModeInfo>();
            gameModes.forEach(gameMode => {
                ret.set(gameMode.id, this.toGameModeInfo(gameMode));
            });
            this.idToGameMode = ret;
            return true;
        });
    }

    private async loadLeaderboardList() {
        await t(async em => {
            const rows = await em.createQueryBuilder(Rating, 'r')
                .innerJoin('r.gameMode', 'gm')
                .groupBy('gm.id')
                .select('max(r.season) as max_season')
                .addSelect('gm')
                .getRawMany();
            
            this.leaderboardList = [];
            for (let row of rows) {
                this.leaderboardList.push({
                    id: row.gm_id,
                    name: row.gm_name,
                    maxSeason: row.max_season,
                });
            };
            return true;
        });
    }

    private toGameModeInfo(gameMode: GameMode): GameModeInfo {
        return {
            id: gameMode.id,
            json: gameMode.json,
            name: gameMode.name,
        };
    }

    getPresetInfo(presetId: number) {
        return this.presetToGameMode.get(presetId);
    }

    getGameModeInfo(gameModeId: number) {
        return this.idToGameMode.get(gameModeId);
    }

    getPresets() {
        console.log(this.presets);
        return this.presets;
    }

    getLeaderboardList() {
        return this.leaderboardList;
    }
}

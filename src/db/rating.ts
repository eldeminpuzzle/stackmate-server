import { PoolClient } from "pg";
import { Rating } from "../entity/rating";
import { Account } from "../entity/account";
import { Client } from "../client";
import { Context } from "../context";
import { LeaderboardData, RatingData, RatingInfo, RatingUpdateInfo } from "../shared/intf";
import { t } from "./dbService";
import { In } from "typeorm";
const glicko2 = require('glicko2');

export class RatingService {

    private calcSeasonPoints(rating: number, matches: number) {
        return rating / 3000 * 100000 * (1-1/(matches/20+1))
    }

    async submit(gameMode: number, season: number, accountIdsByRanking: number[], clients: Client[], log=true): Promise<Map<Client, RatingUpdateInfo>> {
        return t(async em => {
            // load rating data
            let ratings = await em.find(Rating, {
                where: { 
                    gameMode: {id: gameMode},
                    season: season,
                    account: In(accountIdsByRanking)
                }
            });

            let map = new Map<number, Rating>();
            ratings.forEach(rating => {
                map.set(rating.accountId, rating);
            });
                
            // prepare Glicko2
            let ranking = new glicko2.Glicko2({
                tau : 0.3, // 0.3 ~ 1.2
                rating : 1500,
                rd : 200,
                vol : 0.01,
            });
            let players = [];
            for (let accountId of accountIdsByRanking) {
                if (map.has(accountId)) {
                    let rating = map.get(accountId);
                    players.push(ranking.makePlayer(
                        rating.rating,
                        rating.rd,
                        rating.vol
                    ));
                } else {
                    players.push(ranking.makePlayer());
                }
            }

            // get matches
            let matches = [];
            for (let i = 0; i < players.length - 1; i++) {
                for (let j = i + 1; j < players.length; j++) {
                    matches.push([players[i], players[j], 1]);
                }
            }
            ranking.updateRatings(matches);

            // save new ratings
            let ratingUpdateInfo: Map<Client, RatingUpdateInfo> = new Map();
            let promises: Promise<any>[] = [];
            for (let i = 0; i < accountIdsByRanking.length; i++) {
                let accountId = accountIdsByRanking[i];
                let player = players[i];
                let oldInfo: RatingInfo;
                let newInfo: RatingInfo;
                if (map.has(accountId)) {
                    let rating = map.get(accountId);
                    oldInfo = this.toRatingInfo(rating);

                    rating.rating = player.getRating();
                    rating.rd = player.getRd();
                    rating.vol = player.getVol();
                    rating.matches++;
                    await em.save(rating);
                    // promises.push(em.save(rating));
                    
                    newInfo = this.toRatingInfo(rating);
                } else {
                    oldInfo = null;

                    let rating = em.create(Rating, {
                        id: null,
                        account: { id: accountId },
                        gameMode: { id: gameMode },
                        season: season,
                        rating: player.getRating(),
                        rd: player.getRd(),
                        vol: player.getVol(),
                        matches: 1,
                    });

                    promises.push(em.insert(Rating, rating));
                    newInfo = this.toRatingInfo(rating);
                }

                if (log) {
                    Context.get().getDiscordBot().sendSecret(`Rating ${gameMode}: ${clients[i].username} - ${Math.round(player.getRating())} - ${Math.round(player.getRd())} - ${Math.round(player.getVol() * 1000) / 1000}`);
                }

                ratingUpdateInfo.set(clients[i], {
                    old: oldInfo,
                    new: newInfo,
                });
            }

            await Promise.all(promises);
            return ratingUpdateInfo;
        });
    }

    private toRatingInfo(rating: Rating): RatingInfo {
        return {
            ratingData: {
                matches: rating.matches,
                rating: rating.rating,
                rd: rating.rd,
                vol: rating.vol,
            },
            seasonPoints: this.calcSeasonPoints(rating.rating, rating.matches + 1)
        };

    }
    
    async loadLeaderboard(gameModeId: number, season: number, offset: number, limit: number): Promise<LeaderboardData> {
        return t(async em => {
            let ratings = await em.find(Rating, {
                where: {
                    gameMode: {id: gameModeId},
                    season: season,
                },
                relations: ['account'],
                skip: offset,
                take: limit,
            });

            // return rating / 3000 * 100000 * (1-1/(matches/20+1))
            let ret: LeaderboardData = {
                gameModeName: Context.get().getGameRuleService().getGameModeInfo(gameModeId).name,
                offset,
                season,
                rows: [],
            };
            for (let rating of ratings) {
                ret.rows.push({
                    username: rating.account.nameLower,
                    score: this.calcSeasonPoints(rating.rating, rating.matches),
                });
            }
            return ret;
        });
    }
}

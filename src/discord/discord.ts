
import {Channel, Client, Message} from "discord.js";
import { RandomGen } from "../shared/randomGen";
import { Message as ChatMessage } from "../shared/chat/message";

const client = new Client();

export class DiscordBot {

    private TOKEN: string;
    private CHANNEL_ID: string;
    private SECRET_CHANNEL_ID: string;

    private channel: any; // @types is incomplete for Channel
    private secretChannel: any;
    private r = new RandomGen();

    private messages: ChatMessage[] = [];
    private secretMessages: string[] = [];

    constructor() {
        if (this.TOKEN == process.env.DISCORD_TOKEN) {
            this.TOKEN = 'ODA0MDMwNzE1ODIxMTYyNTA4.YBGZ9A.rVtZjHAK3GJ6dh68F_fKKQA2VlY';
            this.CHANNEL_ID = '804043290223312916';
            this.SECRET_CHANNEL_ID = '804953175944855584';
        } else {
            this.TOKEN = process.env.DISCORD_TOKEN;
            this.CHANNEL_ID = '804030590734041109';
            this.SECRET_CHANNEL_ID = '804468080771334275';
        }
        client.login(this.TOKEN);

        
        client.on('ready', () => {
            console.log('ready');
            client.channels.fetch(this.CHANNEL_ID).then((channel: any) => {
                this.channel = channel;
                this.messages.forEach(m => {
                    this.sendMessageDirect(m);
                });
            });
            client.channels.fetch(this.SECRET_CHANNEL_ID).then((channel: any) => {
                this.secretChannel = channel;
                this.secretMessages.forEach(m => {
                    this.sendSecretDirect(m);
                });
            });
        });
    }

    sendMessage(msg: ChatMessage) {
        if (this.channel == null) {
            this.messages.push(msg); // not ready yet
        } else {
            this.sendMessageDirect(msg);
        }
    }

    sendSecret(msg: string) {
        if (this.channel == null) {
            this.secretMessages.push(msg); // not ready yet
        } else {
            this.sendSecretDirect(msg);
        }
    }

    private sendMessageDirect(msg: ChatMessage) {
        this.channel.send('**' + msg.username + '**' + '\n' + msg.message);
    }

    private sendSecretDirect(msg: string) {
        this.secretChannel.send(msg);
    }
}

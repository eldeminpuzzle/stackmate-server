import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Account {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    nameLower: string;

    @Column()
    password: string;

    @Column({ default: 0 })
    coins: number;

    @Column({ default: false })
    active: boolean;
}

import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class GameMode {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    dsc: string;

    @Column()
    json: string;
}

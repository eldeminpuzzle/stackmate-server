import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { GameMode } from "./gameMode";

@Entity()
export class Preset {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    position: number;

    @ManyToOne(() => GameMode, gm => gm.id)
    gameMode: GameMode;

    @RelationId((p: Preset) => p.gameMode)
    gameModeId: number;

}

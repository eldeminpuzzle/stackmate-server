import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { Account } from "./account";
import { GameMode } from "./gameMode";

@Entity()
export class Rating {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => GameMode, gm => gm.id)
    gameMode: GameMode;

    @RelationId((r: Rating) => r.gameMode)
    gameModeId: number;

    @Column()
    season: number;

    @ManyToOne(() => Account, a => a.id)
    account: Account;

    @RelationId((r: Rating) => r.account)
    accountId: number;

    @Column({ type: 'double precision' })
    rating: number;

    @Column({ type: 'double precision' })
    rd: number;

    @Column({ type: 'double precision' })
    vol: number;

    @Column()
    matches: number;
}

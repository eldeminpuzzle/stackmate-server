import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { GameMode } from "./gameMode";

@Entity()
export class ServerRoom {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    position: number;

    @ManyToOne(() => GameMode, gm => gm.id)
    gameMode: GameMode;

    @RelationId((sr: ServerRoom) => sr.gameMode)
    gameModeId: number;

    @Column()
    season: number;
}

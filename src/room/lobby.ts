
import { DataUtils } from "../dataUtils";
import { Client } from "../client";
import { Room } from "./room";
import { RoomInfo } from "../shared/intf";
import { Context } from "../context";
import { GameModeInfo } from "../db/gameRuleService";

export class LobbyService {

    private nextRoomId = 20000;

    private rooms: Map<number, Room> = new Map();

    private clients: Set<Client> = new Set();

    addClient(client: Client) { this.clients.add(client); }
    removeClient(client: Client) { this.clients.delete(client); }
    broadcastToAllClients(name: string, ...args: any) {
        this.clients.forEach(c => c.emit(name, ...args));
    }

    getRoomInfos(): RoomInfo[] {
        let rooms = Array.from(this.rooms.values());

        let roomInfos: RoomInfo[] = [];
        for (let room of rooms) {
            const roomData = room.getRoomData();
            roomInfos.push({
                id: roomData.id,
                name: roomData.name,
                presetName: roomData.presetName,
                serverRoom: roomData.serverRoom,
                host: roomData.host,
                hostIsSpectator: roomData.hostIsSpectator,
                players: roomData.players.map(DataUtils.getPlayerInfo),
                spectators: roomData.spectators.map(DataUtils.getPlayerInfo),
                running: room.isRunning(),
                isHost: false,
                seatNr: null,
                custom: roomData.custom,
                maxPlayers: roomData.maxPlayers,
                autoStart: roomData.autoStart,
                autoJoin: roomData.autoJoin,
            });
        }
        return roomInfos;
    }

    getRoom(id: number) {
        return this.rooms.get(id);
    }

    createRoom(name: string, host: Client, presetId: number, custom: boolean): Room {
        let gameModeInfo = Context.get().getGameRuleService().getPresetInfo(presetId);
        let room = new Room(name, host, this.nextRoomId++, gameModeInfo, custom, true, null, false, null);
        this.rooms.set(room.getRoomData().id, room);
        return room;
    }

    createServerRoom(name: string, season: number, gameModeInfo: GameModeInfo): Room {
        let room = new Room(name, null, this.nextRoomId++, gameModeInfo, false, true, 10, true, season);
        this.rooms.set(room.getRoomData().id, room);
        return room;
    }
    
    exitRoom(roomId: number, player: Client) {
        let room = this.rooms.get(roomId);
        if (room != null) {
            let roomBecomesEmpty = room.exitRoom(player);
            if (roomBecomesEmpty && !room.getRoomData().serverRoom) {
                this.rooms.delete(roomId);
            }
        }
    }
}

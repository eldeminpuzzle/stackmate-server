import { DataUtils } from "../dataUtils";
import { Client } from "../client";
import { RoomData } from "./roomData";
import { RoomInfo, PlayerInfo, GameResult, RatingUpdateInfo, PresetInfo } from "../shared/intf";
import { ServerEngine } from "../serverEngine";
import { GameRule } from "../shared/gameRule";
import { Command, Commands, DieCommand, StartGameData } from "../shared/gameIntf";
import { MoveInfo, SubmoveInfo } from "../shared/playerEngine";
import { SideData } from "../shared/gameData";
import gameRuleUtils from "../shared/gameRuleUtils";
import { Context } from "../context";
import { RandomGen } from "../shared/randomGen";
import config from "../config";
import { GameModeInfo } from "../db/gameRuleService";

export class Room {
    private data: RoomData;
    private serverEngine: ServerEngine;
    gameRuleJson: string;

    autoStartTimeout: any;

    r = new RandomGen();

    private currentGamePlayers: Client[] = []; // data.players can change as players enter/leave. This variable can only change when starting a game and remains fixed until the next game.

    constructor(name: string, host: Client, id: number, gameModeInfo: GameModeInfo, custom: boolean, autoJoin: boolean, autoStart: number, serverRoom: boolean, season: number) {
        this.data = {
            id,
            name,
            presetName: gameModeInfo.name,
            host: 0,
            hostIsSpectator: false,
            players: [host, null],
            spectators: [],
            custom,
            maxPlayers: 0,

            autoJoin,
            autoStart,

            serverRoom,
            gameMode: gameModeInfo.id,
            season,
        };

        this.gameRuleJson = gameModeInfo.json;
    }

    isRunning() {
        return this.serverEngine != null && this.serverEngine.isRunning();
    }

    isRequiredPlayersReady() {
        if (this.data.autoJoin) return this.data.players.findIndex(c => c != null) != -1;
        for (let i = 0; i < this.data.players.length; i++) {
            if (this.data.players[i] == null) {
                return false;
            }
        }
        return true;
    }

    getRoomData() { return this.data; }

    getRoomInfo(client: Client): RoomInfo {
        // transform player into playerInfo
        let playerInfos: PlayerInfo[] = this.data.players.map(DataUtils.getPlayerInfo);
        let spectatorInfos: PlayerInfo[] = this.data.spectators.map(DataUtils.getPlayerInfo);

        let seatNr = this.data.players.findIndex(c => c == client);
        if (seatNr == -1) seatNr = null;

        return {
            id: this.data.id,
            name: this.data.name,
            presetName: this.data.presetName,
            host: this.data.host,
            serverRoom: this.data.serverRoom,
            hostIsSpectator: this.data.hostIsSpectator,
            running: this.isRunning(),
            players: playerInfos,
            spectators: spectatorInfos,
            isHost: this.isHost(client),
            seatNr,
            custom: this.data.custom,
            maxPlayers: this.data.maxPlayers,

            autoJoin: this.data.autoJoin,
            autoStart: this.data.autoStart,
        };
    }

    broadcast(event: string, excludeClient: Client, ...data: any[]) {
        for (let clientList of [this.data.players, this.data.spectators]) {
            for (let i = 0; i < clientList.length; i++) {
                let c = clientList[i];
                if (c != null && c != excludeClient) {
                    c.emit(event, ...data);
                }
            }
        }
    }

    private broadcastCommand(commands: Command[], excludeSideNr: number=null) {
        for (let clientList of [this.data.players, this.data.spectators]) {
            for (let i = 0; i < clientList.length; i++) {
                if (clientList==this.data.players && excludeSideNr == i) continue;
    
                let c = clientList[i];
                if (c != null) {
                    c.emit('gameCommand', commands);
                }
            }
        }
    }



    start() {
        if (this.autoStartTimeout != null) {
            clearTimeout(this.autoStartTimeout);
        }

        if (this.data.autoJoin) {
            // trim players
            let gri = gameRuleUtils.loadRuleJson(this.gameRuleJson);
            for (let i = 0; i < this.data.players.length; i++) {
                if (this.data.players[i] == null) {
                    let swapPlayer = false;
                    for (let j = this.data.players.length - 1; j > i; j--) {
                        if (this.data.players[j] != null) {
                            this.data.players[i] = this.data.players[j];
                            this.data.players[j] = null;
                            gri.sideRules[i] = gri.sideRules[j];
                            swapPlayer = true;
                            break;
                        }
                    }

                    if (!swapPlayer) {
                        this.data.players.splice(i);
                        gri.sideRules.splice(i);
                        this.gameRuleJson = gameRuleUtils.getRuleJson(gri);
                        this.broadcastUpdateGameRule();
                        this.broadcastRoomInfo();
                        break;
                    }
                }
            }
        }

        this.currentGamePlayers = this.data.players.slice();

        let gameRule = gameRuleUtils.getGameRuleFromJson(this.gameRuleJson);
        if (this.serverEngine != null) {
            this.serverEngine.dispose();
        }
        this.serverEngine = new ServerEngine({gameRule, localSideNr: null}, this.broadcastCommand.bind(this), this.disconnectPlayer.bind(this));

        this.serverEngine.listenersOnGameEnded.add(this.onGameEnded.bind(this));

        // emit game start
        for (let i = 0; i < this.data.players.length; i++) {
            let startGameData: StartGameData = {gameRule, localSideNr: i};
            if (this.data.players[i] != null) {
                this.data.players[i].emit('gameEntered', startGameData);
            }
        }
        let startGameData: StartGameData = {gameRule, localSideNr: null};
        this.data.spectators.forEach(s => s.emit('gameEntered', startGameData));

        // emit round start after countdown
        setTimeout((() => {
            this.serverEngine.startRound();
        }), 1000);
    }

    onGameEnded() {
        let isServerRoom = this.data.serverRoom;

        // update rating
        let ratingUpdateInfoMapPromise: Promise<Map<Client, RatingUpdateInfo>> = Promise.resolve(new Map());
        if (isServerRoom) {
            // find rated players
            let ratedPlayers: {client: Client, account: number, rank: number, name: string}[] = [];
            for (let i = 0; i < this.currentGamePlayers.length; i++) {
                let sideStats = this.serverEngine.gameEngine.gameData.stats.sides[i];
                let player = this.currentGamePlayers[i];

                if (player.accountId != null) {
                    ratedPlayers.push({client: player, account: player.accountId, rank: sideStats.rank, name: player.username});
                }
            }

            console.log('update Ratings');
            // update ratings
            if (ratedPlayers.length > 1) {
                ratedPlayers.sort((a, b) => a.rank - b.rank);
                ratingUpdateInfoMapPromise = Context.get().getRatingService().submit(
                    this.data.gameMode,
                    this.data.season,
                    ratedPlayers.map(t => t.account),
                    ratedPlayers.map(t => t.client)
                );
            }
            console.log('updated');
        }

        // game result & reward
        let rankingHelper = [];
        for (let i = 0; i < this.currentGamePlayers.length; i++) {
            rankingHelper.push({ index: i, rank: this.serverEngine.gameEngine.gameData.stats.sides[i].rank });
        }
        rankingHelper.sort((a,b) => a.rank - b.rank);
        let ranking = rankingHelper.map(({index, rank}) => DataUtils.getPlayerInfo(this.currentGamePlayers[index]));

        // copy so that the original array can be modified during async call
        let currentGamePlayers = this.currentGamePlayers.slice();
        let serverEngine = this.serverEngine;
        ratingUpdateInfoMapPromise.then(ratingUpdateInfoMap => {
            for (let i = 0; i < currentGamePlayers.length; i++) {
                let sideStats = serverEngine.gameEngine.gameData.stats.sides[i];
                let player = currentGamePlayers[i];
    
                let coinReward = 0;
                if (sideStats.totalPieces < config.afkPieceLimit) {
                    // afk player, no reward
    
                    if (isServerRoom) {
                        // kick if loser
                        if (sideStats.rank > 0) {
                            if (player == this.data.players[i]) {
                                this.onSwitchSeatNr(player, null);
                            }
                        }
                    }
                } else if (player.accountId != null) {
                    // reward coin
                    let turnBasedFactor = serverEngine.gameEngine.gameRule.sides[0].turnBased ? 0.5 : 1;
                    let serverRoomFactor = isServerRoom ? 1 : 0.4;
                    let numPlayersFactor = 0.1 + Math.max(0, 0.9 * 1-1/((serverEngine.gameEngine.gameRule.sides.length+1)/2));
                    let timeFactor = Math.pow(Math.min(300, sideStats.totalTime) / 5, 0.9);
                    let piecesFactor = 1 - 1 / (sideStats.totalPieces / 35 + 1);
                    let rFactor = this.r.range(0.5, 1.5);
                    let multiplier = 6;
                    let multiplierAfterRounding = 5;
                    coinReward = (Math.floor(turnBasedFactor * serverRoomFactor * numPlayersFactor * timeFactor * piecesFactor * rFactor * multiplier) + 1) * multiplierAfterRounding;
                    Context.get().getDiscordBot().sendSecret(`Coin: ${player.username} - ${coinReward}`);
                    Context.get().getAccountService().addCoins(player.username, coinReward);
                }
    
                let ratingUpdateInfo = ratingUpdateInfoMap.get(player);

                let gameResult: GameResult = {
                    roomId: this.data.id,
                    coinReward,
                    ratingUpdateInfo,
                    ranking,
                };

                // if the player is still around, send GameResult
                if (this.data.players.indexOf(player) != -1){
                    player.emit('gameEnded', gameResult);
                }
            }

            let gameResultSpectator: GameResult = { roomId: this.data.id, ranking };
            this.data.spectators.forEach(s => {
                s.emit('gameEnded', gameResultSpectator);
            });
            this.data.players.forEach(s => {
                if (s != null && this.currentGamePlayers.indexOf(s) == -1) {
                    s.emit('gameEnded', gameResultSpectator);
                }
            });
        });

        this.checkAutoStart();
    }

    onMoveReceived(moveInfo: MoveInfo, source: Client) {
        let sourceId = this.currentGamePlayers.indexOf(source);
        if (sourceId != -1) {
            this.serverEngine.onMoveReceived(moveInfo, sourceId);
        }
    }

    onSubMoveReceived(subMoveInfo: SubmoveInfo, source: Client) {
        let sourceId = this.currentGamePlayers.indexOf(source);
        if (sourceId != -1) {
            this.serverEngine.onSubMoveReceived(subMoveInfo, sourceId);
        }
    }

    onDieReceived(source: Client) {
        let sourceId = this.currentGamePlayers.indexOf(source);
        this.serverEngine.diePlayer(sourceId);
    }

    onUpdateGameRuleReceived(gameRule: GameRule, json: string) {
        // move excess players into spectator
        for (let i = gameRule.sides.length; i < this.data.players.length; i++) {
            if (this.data.players[i] != null) {
                this.data.spectators.push(this.data.players[i]);

                // host was moved
                if (!this.data.hostIsSpectator && this.data.host == i) {
                    this.data.host = this.data.spectators.length - 1;
                    this.data.hostIsSpectator = true;
                }
            }
        }

        // update player number
        this.data.players.length = gameRule.sides.length;
        this.gameRuleJson = json;

        this.checkAutoStart();

        this.broadcastUpdateGameRule();
    }
    
    onSwitchSeatNr(source: Client, sideNr: number) {
        let wasHost = this.isHost(source);
        let success = false;
        let playerId = this.data.players.indexOf(source);
        let spectatorId = this.data.spectators.indexOf(source);
        if (sideNr == null) {
            if (playerId != -1) {
                // player becomes a spectator
                this.data.spectators.push(source);
                this.data.players[playerId] = null;
                if (wasHost) {
                    this.data.host = this.data.spectators.length - 1;
                    this.data.hostIsSpectator = true;
                }
                success = true;
            } // else: error, spectator already a spectator
        } else if (this.data.players[sideNr] == null) {
            if (playerId == -1) {
                // spectator becomes a player
                this.data.players[sideNr] = source;
                this.data.spectators.splice(spectatorId, 1)[0];
                if (wasHost) {
                    this.data.host = sideNr;
                    this.data.hostIsSpectator = false;
                }
                success = true;
                this.checkAutoStart();
            } else if (playerId != sideNr) {
                // player switches seat
                this.data.players[playerId] = null;
                this.data.players[sideNr] = source;
                if (wasHost) {
                    this.data.host = sideNr;
                    this.data.hostIsSpectator = false;
                }
                success = true;
            } // else: error, player is already in that seat
        } // else: reject, seat is already taken (might be due to race condition)

        if (success) {
            this.broadcastRoomInfo();
        }
    }

    onTransferHost(source: Client, newHost: number, newHostIsSpectator: boolean) {
        if (this.isHost(source)) {
            if ((newHostIsSpectator && this.data.spectators.length >= newHost-1)
                || (!newHostIsSpectator && this.data.players[newHost] != null)) {
                this.data.host = newHost;
                this.data.hostIsSpectator = newHostIsSpectator;
                this.broadcastRoomInfo();
            } // else: reject, new host is an empty seat
        } // else: reject, not allowed because source is not host (might be due to double submit)
    }

    isHost(client: Client) {
        if (this.data.serverRoom) return false;

        return (this.data.hostIsSpectator && this.data.spectators.indexOf(client) == this.data.host)
            || (!this.data.hostIsSpectator && this.data.players.indexOf(client) == this.data.host);
    }

    exitRoom(client: Client, switchOnly=false): boolean { // returns true if room becomes empty!
        let wasHost = this.isHost(client);

        // remove from player list
        this.removePlayerFromLists(client);

        if (!switchOnly) {
            if (wasHost) { // replace host if host left
                this.assignAnyHost();
            }
        }

        // return true if room becomes empty
        let empty = true;
        if (this.data.players.findIndex(p => p != null) != -1) empty = false;
        if (this.data.spectators.length > 0) empty = false;

        if (!switchOnly) {
            this.broadcastRoomInfo();
        }
        return empty;
    }

    private removePlayerFromLists(client: Client) {
        let currentPlayers = this.currentGamePlayers;
        for (let i = 0; i < currentPlayers.length; i++) {
            if (currentPlayers[i] != null) {
                if (currentPlayers[i].id == client.id) {
                    // if room is running and the player was alive, kill him
                    if (this.serverEngine != null && this.serverEngine.gameEngine.gameData.sides[i].isAlive) {
                        this.serverEngine.exitPlayer(i);
                    }
                }
            }
        }


        let players = this.data.players;
        for (let i = 0; i < players.length; i++) {
            if (players[i] != null) {
                if (players[i].id == client.id) {
                    players[i] = null;
                }
            }
        }
        

        // remove from spectator list
        let spectators = this.data.spectators;
        let sid = spectators.indexOf(client);
        if (sid != -1) {
            spectators.splice(sid, 1);
            if (this.data.hostIsSpectator && this.data.host > sid) {
                this.data.host--;
            }
        }
    }

    broadcastRoomInfo(excludeClient: Client=null) {
        for (let clientList of [this.data.players, this.data.spectators]) {
            for (let i = 0; i < clientList.length; i++) {
                if (clientList[i] != null && clientList[i] != excludeClient) {
                    clientList[i].emit('updateRoomInfo', this.getRoomInfo(clientList[i]));
                }
            }
        }
    }

    private broadcastUpdateGameRule(excludeClient: Client=null) {
        for (let clientList of [this.data.players, this.data.spectators]) {
            for (let i = 0; i < clientList.length; i++) {
                let client = clientList[i];
                if (client != null && client != excludeClient) {
                    if (!this.isHost(client)) {
                        client.emit('updateGameRule', this.gameRuleJson, this.getRoomInfo(client));
                    } else {
                        client.emit('updateRoomInfo', this.getRoomInfo(client));
                    }
                }
            }
        }
    }

    assignAnyHost() {
        for (let i = 0; i < this.data.players.length; i++) {
            if (this.data.players[i] != null) {
                this.data.host = i;
                this.data.hostIsSpectator = false;
                return;
            }
        }

        if (this.data.spectators.length > 0) {
            this.data.host = 0;
            this.data.hostIsSpectator = true;
        }
    }

    onDebugData(source: Client, sideData: SideData) {
        let sourceId = this.currentGamePlayers.indexOf(source);
        
        let oos = false;
        for (let i = 0; i < sideData.board.length; i++) {
            for (let j = 0; j < sideData.board[j].length; j++) {
                let expected = sideData.board[i][j];
                let actual = this.serverEngine.gameEngine.playerEngines[sourceId].sideData.board[i][j];
                if ((expected==null) != (actual==null) 
                || (expected != null && (expected.colorId != actual.colorId || expected.isGarbage != actual.isGarbage))) {
                    console.log(expected);
                    console.log(actual);

                    console.log('Out of sync detected!!! Server state:');
                    this.serverEngine.printBoard(sourceId);

                    console.log('\n\n received state:');
                    this.serverEngine.printBoard(null, sideData.board);
                    oos = true;
                    break;
                }
                if (oos) break;
            }
        }
    }

    onSetAutoJoin(autoJoin: boolean) {
        this.data.autoJoin = autoJoin;
        this.broadcastRoomInfo();
    }

    onSetAutoStart(autoStart: number) {
        this.data.autoStart = autoStart;

        this.checkAutoStart();
        this.broadcastRoomInfo();
    }

    checkAutoStart() {
        // if no players at all, do not start
        if (this.data.players.findIndex(p => p != null) == -1) {
            console.log('no players');
            return;
        }

        // if not auto join and some slots are empty, do not start
        if (!this.data.autoJoin && this.data.players.findIndex(p => p == null) != -1) {
            console.log('not enough players');
            return;
        }

        let shouldAutoStart = this.data.autoStart != null && !this.isRunning();
        if (this.autoStartTimeout == null && shouldAutoStart) {
            console.log('auto start in ' + this.data.autoStart + ' seconds');
            this.autoStartTimeout = setTimeout(() => {
                this.autoStartTimeout = null;

                // if no players at all, do not start
                if (this.data.players.findIndex(p => p != null) == -1) {
                    console.log('cancel autostart because there is no player');
                    return;
                }

                // if not auto join and some slots are empty, do not start
                if (!this.data.autoJoin && this.data.players.findIndex(p => p == null) != -1) {
                    console.log('cancel autostart due to empty slots');
                    return;
                }

                this.start();
            }, this.data.autoStart * 1000);
        } else if (this.autoStartTimeout != null && !shouldAutoStart) {
            console.log('clear time out');
            clearTimeout(this.autoStartTimeout);
            this.autoStartTimeout = null;
        }
    }
    



    
    enterRoom(client: Client, spectateMode: boolean): number {
        let players = this.data.players;
        let spectators = this.data.spectators;

        if (this.data.autoJoin && !spectateMode) {
            return this.autoJoin(client, false);
        } else {
            let seatNr = null;
            if (!spectateMode) {
                // find free seat. If none, then join as spectator (seatNr == null)
                for (let i = 0; i < players.length; i++) {
                    if (players[i] == null) {
                        seatNr = i;
                        break;
                    }
                }
            }

            // update player or spectator list
            if (seatNr == null) {
                spectators.push(client);
            } else {
                players[seatNr] = client;
                this.checkAutoStart();
            }
            return seatNr;
        }
    }

    autoJoin(client: Client, notifyClient=true) {
        let wasHost = this.isHost(client);

        this.removePlayerFromLists(client);
        
        // try to enter an empty slot
        let newSlot = null;
        for (let i = 0; i < this.data.players.length; i++) {
            if (this.data.players[i] == null) {
                newSlot = i;
                break;
            }
        }

        // no empty slot -> add new slot
        if (newSlot == null) {
            let gri = gameRuleUtils.loadRuleJson(this.gameRuleJson);
            gri.sideRules.push(new Map());
            this.gameRuleJson = gameRuleUtils.getRuleJson(gri);

            newSlot = gri.sideRules.length - 1;
            // this.broadcastUpdateGameRule(notifyClient ? null : client);
            this.broadcast('playerAutoJoined', notifyClient ? null : client);
        }
        
        // assign slot
        this.data.players[newSlot] = client;
        if (wasHost) {
            this.data.hostIsSpectator = false;
            this.data.host = newSlot;
        }

        this.broadcastRoomInfo(notifyClient ? null : client);

        // if auto join and only 1 player, abort the game
        if (this.currentGamePlayers.length == 1 && this.serverEngine.isRunning()) {
            this.serverEngine.diePlayer(0);
        }

        this.checkAutoStart();
        return newSlot;
    }

    private disconnectPlayer(sideNr: number) {
        console.log('force disconnect due to submove');
        let cmd: DieCommand = { sideNr, type: Commands.DIE };
        this.broadcastCommand([cmd], null);
    }
}

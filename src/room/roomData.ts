
import { Client } from "../client";

export interface RoomData {
    id: number;
    name: string;
    host: number;
    hostIsSpectator: boolean;
    players: Client[];
    spectators: Client[];
    maxPlayers: number;

    custom: boolean;
    autoStart: number;
    autoJoin: boolean;
    
    serverRoom: boolean;
    gameMode: number;
    presetName: string;
    season: number;
}

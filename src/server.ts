import * as express from "express";
import { Socket } from "socket.io";
import { createConnection } from "typeorm";
import { Client } from "./client";
import { Context } from "./context";
import { t } from "./db/dbService";
import { GameModeInfo } from "./db/gameRuleService";
import { ServerRoom } from "./entity/serverRoom";
import { SocketWrapper } from "./shared/socketWrapper";


createConnection().then(() => {
    Context.get().init().then(async () => {
        const app = express();
        const port = process.env.PORT || 80;
        app.set("port", port);
        
        let http = require("http").Server(app);
        // set up socket.io and bind it to our
        // http server.
        let io = require("socket.io")(http);
        
        // app.get("/", (req: any, res: any) => {
        //     res.sendFile(path.resolve("./client/index.html"));
        // });
        app.use(express.static('tbs-client'))
        
        // whenever a user connects on port 3000 via
        // a websocket, log that a user has connected
        io.on("connection", function (socket: Socket) {
            new Client(new SocketWrapper(socket));
        });
        
        const server = http.listen(port, function () {
            console.log("listening on *:" + port);
        });
        
        process.on('uncaughtException', function(err){
            console.error(err);
        });
        
        
        
        // init room
        await t(async em => {
            const serverRooms = await em.find(ServerRoom, {
                relations: ['gameMode']
            });

            for (let serverRoom of serverRooms) {
                let gameModeInfo: GameModeInfo = {
                    id: serverRoom.id,
                    json: serverRoom.gameMode.json,
                    name: serverRoom.gameMode.name,
                };
                Context.get().getLobbyService().createServerRoom('Free for all', serverRoom.season, gameModeInfo);
            }
            
            console.log('Server rooms created');
        });
    }).catch(r => {
        console.error(r);
    });
});
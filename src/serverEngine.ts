
import { GameEngine } from "./shared/gameEngine";
import { StartGameData, Command, Commands, StartRoundCommand, SpawnGarbageCommand, PlacePieceCommand, DieCommand, TimeOutCommand, SetTimeCommand, TileType, SubMoveCommand } from "./shared/gameIntf";
import { MoveInfo, SubmoveInfo } from "./shared/playerEngine";
import { GarbageHoleData, TargetingMode } from "./shared/gameRule";
import { RandomGen } from "./shared/randomGen";
import { Tile } from "./shared/gameData";
import { runInThisContext } from "vm";
import { Client } from "./client";

export class ServerEngine {

    public listenersOnGameEnded = new Set<() => any>();

    public gameEngine: GameEngine;
    private receiveSurplus: number[] = [];
    private disconnectTimeout: any[] = [];
    private playerStillThere: boolean[] = [];

    public roundEnded = false;
    private gameEnded = false;

    private checkClockTimeout: any = null;

    private r = new RandomGen();

    constructor(
        startGameData: StartGameData,
        private broadcastCommand: (commands: Command[], excludeSideNr?: number) => any,
        private disconnectPlayer: (seatNr: number) => any,
    ) {
        this.gameEngine = new GameEngine(startGameData, true);
        
        this.gameEngine.listenersOnRoundEnded.add(this.onRoundEnded.bind(this));

        for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
            this.playerStillThere.push(true);
        }
    }

    isRunning() {
        return !this.gameEnded;
    }

    dispose() {
        if (this.checkClockTimeout != null) {
            clearTimeout(this.checkClockTimeout);
        }
    }
    
    private resetTimeout(sideNr: number) {
        if (this.disconnectTimeout != null) {
            clearTimeout(this.disconnectTimeout[sideNr]);
            this.disconnectTimeout[sideNr] = null;
        }
            
        if (!this.gameEnded && this.gameEngine.gameData.sides[sideNr].isAlive) {
            this.disconnectTimeout[sideNr] = setTimeout(() => this.disconnectPlayer(sideNr), this.gameEngine.gameRule.sides[sideNr].lagTolerance);
        }
    }

    onSubMoveReceived(submoveInfo: SubmoveInfo, sourceId: number) {
        let cmd: SubMoveCommand = {
            type: Commands.SUB_MOVE,
            sideNr: sourceId,
            subMoveInfo: submoveInfo,
        }
        this.broadcastCommand([cmd], sourceId);
        this.resetTimeout(sourceId);
    }

    onMoveReceived(moveInfo: MoveInfo, sourceId: number) {
        this.resetTimeout(sourceId);
        if (!this.gameEngine.gameData.sides[sourceId].isAlive || !this.gameEngine.playerEngines[sourceId].isTurn()) {
            // ignore move from dead / no turn players (could be caused by race condition when the player is running out of time)
            return;
        }

        let command: PlacePieceCommand = {
            type: Commands.PLACE_PIECE,
            sideNr: sourceId,
            moveInfo
        };
        let commands: Command[] = [command];

        this.doMove(sourceId, moveInfo).forEach(cmd => commands.push(cmd));
        // this.refillPreviews().forEach(cmd => commands.push(cmd));

        this.gameEngine.executeCommands(commands.slice(1)); // remove the PLACE_PIECE command (UGLY CODE???)
        if (this.gameEngine.gameData.turn != sourceId) { // if turn has changed, control clock
            let setTimeCommand: SetTimeCommand = {
                type: Commands.SET_TIME,
                sideNr: sourceId,
                time: this.gameEngine.gameData.sides[sourceId].time - (Date.now() - this.gameEngine.gameData.sides[sourceId].turnStart) / 1000
            };
            this.gameEngine.executeCommands([setTimeCommand]);
            commands.push(setTimeCommand);
        }
        this.setNextClockTimeout();
        
        this.broadcastCommand(commands);
    }

    printBoard(sideNr: number, board: Tile[][]=this.gameEngine.gameData.sides[sideNr].board) {
        console.log('Printing board state:');
        for (let i = 0; i < board.length; i++) {
            let row = '';
            for (let j = 0; j < board[i].length; j++) {
                row += (board[i][j] == null ? ' ' : board[i][j].colorId) + ' ';
            }
            console.log(row);
        }
    }

    diePlayer(sideNr: number) {
        let command: DieCommand = {
            type: Commands.DIE,
            sideNr
        }
        let commands: Command[] = [command];
        this.gameEngine.executeCommands(commands);
        this.broadcastCommand(commands);

        if (this.disconnectTimeout != null && this.disconnectTimeout[sideNr] != null) {
            clearTimeout(this.disconnectTimeout[sideNr]);
            this.disconnectTimeout[sideNr] = null;
        }
        console.log('die received');
    }

    exitPlayer(sideNr: number) {
        this.diePlayer(sideNr);
        this.playerStillThere[sideNr] = false;
    }

    setNextClockTimeout() {
        if (this.gameEngine.gameRule.sides[0].turnBased) {
            if (this.checkClockTimeout != null) {
                clearTimeout(this.checkClockTimeout);
            }
            let nextTurn = this.gameEngine.gameData.turn;
            if (nextTurn != null) {
                let timeElapsedThisTurn = Date.now() - this.gameEngine.gameData.sides[nextTurn].turnStart;
                this.checkClockTimeout = setTimeout(() => this.timeRunOut(nextTurn), this.gameEngine.gameData.sides[nextTurn].time * 1000 - timeElapsedThisTurn);
            }
        }
    }

    timeRunOut(sideNr: number) {
        console.log('TIME RUN OUT');
        let cmd: TimeOutCommand = {
            type: Commands.TIME_OUT,
            sideNr
        };
        
        this.broadcastCommand([cmd]);
        this.gameEngine.executeCommands([cmd]);
    }

    onRoundEnded() {
        this.roundEnded = true;
        if (this.checkClockTimeout != null) {
            clearTimeout(this.checkClockTimeout);
            this.checkClockTimeout = null;
        }

        if (this.disconnectTimeout != null) {
            this.disconnectTimeout.forEach(t => clearTimeout(t));
        }
        this.disconnectTimeout = null;
        console.log('timeout cleared');

        // TODO: round points
        if (true) {
            // match is over
            this.gameEnded = true;
            this.listenersOnGameEnded.forEach(l => l());
        } else {
            // next round
            setTimeout(() => {
                let commands = this.startRound();
            }, 3000);
        }
    }

    startRound() {
        // reset
        this.roundEnded = false;

        this.receiveSurplus = [];
        this.disconnectTimeout = [];
        let randomSeeds: number[] = [];
        let sharedSeed = this.r.int();
        for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
            let seed = this.gameEngine.gameRule.sides[i].pieceGenerationShareSeed ? sharedSeed : this.r.int();
            randomSeeds.push(seed);
            this.receiveSurplus.push(0);

            // TODO: if player has left before the round started, the timeout will disconnect him!
            if (this.playerStillThere[i]) {
                this.disconnectTimeout.push(setTimeout(() => this.disconnectPlayer(i), this.gameEngine.gameRule.sides[i].lagTolerance));
            }
        }
        console.log('start round - timeout set');

        // prepare commands
        let commands: Command[] = [];
        let startRoundCommand: StartRoundCommand = {
            type: Commands.START_ROUND,
            turn: this.r.int(this.gameEngine.gameRule.sides.length),
            randomSeeds
        };
        commands.push(startRoundCommand);
        
        this.gameEngine.executeCommands(commands);
        this.broadcastCommand(commands);
        this.setNextClockTimeout();
    }

    doMove(sideNr: number, moveInfo: MoveInfo): Command[] {
        let moveResult = this.gameEngine.playerEngines[sideNr].doMoveInfo(moveInfo);

        // choose target
        let targetSideNr = this.chooseTarget(sideNr);

        let commands: Command[] = [];

        if (targetSideNr != null) {
            for (let power of moveResult.attackPower) {
                for(let receivePower of this.applyReceiveMultiplier(targetSideNr, power)) {
                    if (receivePower != 0) {
                        commands.push(this.createSpawnGarbageCommand(targetSideNr, receivePower, this.gameEngine.gameRule.sides[targetSideNr].garbageHoles));
                    }
                }
            }
        }
            
        // this.printBoard(sideNr);
        return commands;
    }

    applyReceiveMultiplier(sideNr: number, power: number): number[] {
        let mult = this.gameEngine.gameRule.sides[sideNr].receiveMultiplier;
        if (mult == 1) {
            return [power];
        } else if (this.gameEngine.gameRule.sides[sideNr].useAttackMultiplierBalancer) {
            let ret: number[] = [];

            // spawn guaranteed
            for (let i = 0; i < Math.floor(mult); i++) {
                ret.push(power);
            }

            // spawn remainder
            let remainder = mult - Math.floor(mult);
            this.receiveSurplus[sideNr] += remainder * power;
            if (this.receiveSurplus[sideNr] >= power/2) {
                ret.push(power);
                this.receiveSurplus[sideNr] -= power;
            }
            return ret;
        } else {
            return [Math.floor(power * mult)];
        }
    }

    chooseTarget(sideNr: number) {
        let sideRule = this.gameEngine.gameRule.sides[sideNr];
        let team = this.gameEngine.gameRule.sides[sideNr].team;
        let targetSideNr = sideNr;
        let targetingMode = sideRule.targetingMode;

        if (targetingMode == TargetingMode.NEXT) {
            for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
                targetSideNr = (targetSideNr + 1) % this.gameEngine.gameRule.sides.length;
                let targetTeam = this.gameEngine.gameRule.sides[targetSideNr].team;
                if (this.gameEngine.gameData.sides[targetSideNr].isAlive && (targetTeam == 0 || targetTeam != team)) {
                    break;
                }
            }
        } else if (targetingMode == TargetingMode.LEAST) {
            let leasts: number[] = [];
            let leastRecv = 10000;
            for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
                // do not target own team or dead players
                let targetTeam = this.gameEngine.gameRule.sides[i].team;
                if (this.gameEngine.gameData.sides[i].isAlive && i != sideNr && (targetTeam == 0 || targetTeam != team)) {
                    let recv = this.gameEngine.gameData.stats.sides[i].totalReceived;
                    if (recv == leastRecv) {
                        leasts.push(i);
                    } else if (recv < leastRecv) {
                        leasts = [i];
                        leastRecv = recv;
                    }
                }
            }

            // leasts now contains candidates
            if (leasts.length > 0) {
                targetSideNr = this.r.pick(leasts);
            }
        } else if (targetingMode == TargetingMode.LEAST_ON_SCREEN) {
            let leasts: number[] = [];
            let leastRecv = 10000;
            for (let i = 0; i < this.gameEngine.gameRule.sides.length; i++) {
                // do not target own team or dead players
                let targetTeam = this.gameEngine.gameRule.sides[i].team;
                if (this.gameEngine.gameData.sides[i].isAlive && i != sideNr && (targetTeam == 0 || targetTeam != team)) {
                    let recv = this.gameEngine.gameData.sides[i].visibleGarbage;
                    if (recv == leastRecv) {
                        leasts.push(i);
                    } else if (recv < leastRecv) {
                        leasts = [i];
                        leastRecv = recv;
                    }
                }
            }

            // leasts now contains candidates
            if (leasts.length > 0) {
                targetSideNr = this.r.pick(leasts);
            }
        }

        return !sideRule.allowTargetSelf && targetSideNr == sideNr ? null : targetSideNr;
    }
    
    private createSpawnGarbageCommand(targetSideNr: number, height: number, holes: GarbageHoleData[]) {
        let cmd: SpawnGarbageCommand = {
            sideNr: targetSideNr,
            height,
            holes: holes,
            type: Commands.SPAWN_GARBAGE,
        };
        return cmd;
    }
}

import { GameData, GameStats, SideStats } from "./gameData";
import { Command, Commands, StartGameData, StartRoundCommand } from "./gameIntf";
import { GameRule } from "./gameRule";
import { PlayerEngine, MoveInfo, MoveResult, SubmoveInfo } from "./playerEngine";

export class GameEngine {

    playerEngines: PlayerEngine[];

    gameData: GameData;
    gameRule: GameRule;
    localSeatNr: number;

    executor = new Map<Commands, any>();

    roundStartTime: number;

    public listenersOnRoundEnded = new Set<() => any>();
    public listenersOnFrameExecutorAckPushed = new Set<(submoveInfo: SubmoveInfo) => any>();
    
    constructor(
        startGameData: StartGameData, 
        private isServer: boolean // USE VERY SPARINGLY. Currently this flag is only used to determine the playerEngine.isLocal
    ) {
        this.executor.set(Commands.START_ROUND, this.cmd_startRound.bind(this));

        this.gameRule = startGameData.gameRule;
        this.localSeatNr = startGameData.localSideNr;

        // init gameData
        this.gameData = {
            winner: null,
            sides: [],
            turn: 0,
            stats: new GameStats(),
        };
        this.playerEngines = [];
        for (let sideNr = 0; sideNr < this.gameRule.sides.length; sideNr++) {
            let sideRule = this.gameRule.sides[sideNr];
            this.gameData.stats.sides.push(new SideStats());
            
            let playerEngine = new PlayerEngine(this.gameData, sideRule, sideNr, this.gameData.stats.sides[sideNr], isServer ? false : (this.localSeatNr != null && this.localSeatNr == sideNr));
            this.gameData.sides.push(playerEngine.sideData);
            this.playerEngines.push(playerEngine);
            
            this.playerEngines[sideNr].listenersOnLockDown.add((moveInfo: MoveInfo, moveResult: MoveResult) => {
                this.checkTurn();
            });
            this.playerEngines[sideNr].listenersOnLockOut.add(() => {
                this.onLockOut(sideNr);
            });

            this.playerEngines[sideNr].listenersOnFrameExecutorAckPushed.add(submove => {
                this.listenersOnFrameExecutorAckPushed.forEach(l => l(submove));
            })
        }
        this.gameData.stats.maxRank = this.getCurrentRank();
    }

    private getCurrentRank() {
        let teams = new Set<number>();
        let independent = 0;
        for (let sideNr = 0; sideNr < this.gameRule.sides.length; sideNr++) {
            if (!this.gameData.sides[sideNr].isAlive) continue;

            let sideRule = this.gameRule.sides[sideNr];
            if (sideRule.team == 0) {
                independent++;
            } else {
                teams.add(sideRule.team);
            }
        }
        return teams.size + independent - 1;
    }

    // private getCurrentRank() {
        
    //     let victory = false;
    //     if (aliveTeams.size == 0) victory = true;
    //     if (aliveTeams.size == 1 && aliveTeams.values().next().value != 0) victory = true; // 1 team left and not team 0 (independent)
    //     if (this.getAlivePlayers().size <= 1) victory = true;

    // }

    tick(dt: number, ct: number) {
        this.gameData.stats.totalTime += dt;
        this.playerEngines.forEach(p => p.tick(dt, ct));
    }

    onLockOut(deadSideNr: number) {
        let aliveTeams = this.getAliveTeams();
        let currentRank = this.getCurrentRank();

        let ct = Date.now();
        this.gameData.stats.sides[deadSideNr].totalTime = (ct - this.roundStartTime) / 1000;

        let deadTeam = this.gameRule.sides[deadSideNr].team;
        if (deadTeam == 0 || !aliveTeams.has(deadTeam)) {
            // player's team is wiped out - assign rank
            if (deadTeam == 0) {
                this.gameData.stats.sides[deadSideNr].rank = currentRank + 1;
            } else {
                for (let i = 0; i < this.gameRule.sides.length; i++) {
                    if (this.gameRule.sides[i].team == deadTeam) {
                        this.gameData.stats.sides[i].rank = currentRank + 1;
                    }
                }
            }
        }

        // round over
        if (currentRank <= 0) {
            // winning team gets rank = 0
            for (let i = 0; i < this.gameRule.sides.length; i++) {
                if (this.gameData.sides[i].isAlive || (this.gameRule.sides[i].team != 0 && aliveTeams.has(this.gameRule.sides[i].team))) {
                    this.gameData.stats.sides[i].rank = 0;
                }

                if (this.gameData.sides[i].isAlive) {
                    this.gameData.stats.sides[i].totalTime = (ct - this.roundStartTime) / 1000;
                }
            }

            this.gameData.turn = null;
            // TODO: round points before setting winner
            this.gameData.winner = -1; // TODO: 
            
            this.listenersOnRoundEnded.forEach(l => l());
            this.playerEngines.forEach(p => p.listenersOnRoundEnded.forEach(l => l()));
        }
        
        this.checkTurn();
    }

    getAlivePlayers(): Set<number> {
        let alivePlayers = new Set<number>();
        for (let i = 0; i < this.gameData.sides.length; i++) {
            if (this.gameData.sides[i].isAlive) {
                alivePlayers.add(i);
            }
        }
        return alivePlayers;
    }

    getAliveTeams(): Set<number> {
        let aliveTeams = new Set<number>();
        for (let i = 0; i < this.gameData.sides.length; i++) {
            if (this.gameData.sides[i].isAlive) {
                aliveTeams.add(this.gameRule.sides[i].team);
            }
        }
        return aliveTeams;
    }

    checkTurn() {
        if (!this.gameRule.sides[0].turnBased) return;

        if (this.gameData.turn != null && (this.gameData.sides[this.gameData.turn].piecesLeft <= 0 || !this.gameData.sides[this.gameData.turn].isAlive)) {
            if (this.gameData.sides.length > 1) {
                do {
                    this.gameData.turn = (this.gameData.turn + 1) % this.gameData.sides.length;
                } while (!this.gameData.sides[this.gameData.turn].isAlive); // skip dead players
            }

            this.playerEngines[this.gameData.turn].startTurn();
        }
    }

    startTurn(sideNr: number) {
        this.playerEngines[sideNr].startTurn();
    }

    executeCommands(commands: Command[]) {
        let triggeredPlayerEngines = new Set<PlayerEngine>();
        for (let cmd of commands) {
            let gameExecutor = this.executor.get(cmd.type);
            if (gameExecutor != null) {
                gameExecutor(cmd);
            }
            
            if (cmd.sideNr != null) {
                this.playerEngines[cmd.sideNr].executeCommand(cmd);
                triggeredPlayerEngines.add(this.playerEngines[cmd.sideNr]);
            }
        }

        triggeredPlayerEngines.forEach(pe => { pe.triggerListenerOnCommandExecuted(); })
    }

    



    cmd_startRound(cmd: StartRoundCommand) {
        // reset game data
        this.gameData.winner = null;
        this.gameData.turn = cmd.turn;
        this.roundStartTime = Date.now();
        for (let i = 0; i < this.playerEngines.length; i++) {
            this.playerEngines[i].startRound(cmd.randomSeeds[i]);
        }

        if (this.gameRule.sides[0].turnBased) {
            this.startTurn(this.gameData.turn);
        }
    }
}

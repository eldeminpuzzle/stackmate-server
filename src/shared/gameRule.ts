import { TileType } from "./gameIntf";
import { PieceList } from "./pieceGen";

export interface GameRule {
    sides: SideRule[];
}

export interface TimeRule {
    initialSeconds: number;
    increment: number;
    maxPool: number;
}

export interface GarbageHoleData {
    width: number;
    type: TileType;
}

export interface SideRule {
    levelScript: string;

    width: number;
    height: number;
    playHeight: number;
    numPreviews: number;
    rotationMode: RotationMode;
    holdMode: HoldMode;
    allow180Rotation: boolean;

    team: number;
    targetingMode: TargetingMode;
    allowTargetSelf: boolean;

    // turn control
    turnBased: boolean;
    piecesPerTurn: number;
    timeInitial: number;
    timePerTurn: number;
    timePerMove: number;
    timeMaxPool: number;

    // piece gen
    generatePieceLocally: boolean;
    pieceGenerationShareSeed: boolean;
    pieceGenerationMode: PieceGenerationMode;
    pieceGenerationMemSize: number;
    pieceList: PieceList[];

    // garbage blocking
    garbageEnterDelay: number;
    lineClearDelaysGarbage: boolean;
    garbageSpawnCap: number;
    garbageSpawnFixRate: number;
    garbageBlockingMode: GarbageBlockingMode;

    // garbage generation rules
    garbageHoles: GarbageHoleData[];
    garbageCleanlinessWithin: number;
    garbageCleanlinessBetween: number;
    garbageEdgeLimitation: number;

    // bombs/locks garbage rules
    locksComboMode: LockComboMode;
    locksMaxMultiClearValue: number;

    // gravity
    gravity: number;
    lockDelay: number;
    lockDelayResetLimit: number;

    comboMode: ComboMode;
    comboTable: number[];
    multiClearTable: number[];
    spinBonusTableBase0: number[];

    b2bMultiplierTable: number[];
    b2bAdditiveTable: number[];
    b2bTruthTable: number[];
    b2bSpinTruthTableBase0: number[];
    
    spinBonusMode: SpinBonusMode;
    perfectClearBonus: number;
    colorClearBonus: number;
    
    // combo timer (cultris style)
    comboTimerInitial: number;
    comboTimerMultiClearBonusBase0: number[];
    comboTimerSpinBonusBase0: number[];
    comboTimerTimeBonusMultiplierTable: number[]; // multiplier is not applied if time bonus is negative (e.g. penalty for not clearing lines)

    attackMultiplier: number;
    receiveMultiplier: number;
    useAttackMultiplierBalancer: boolean;

    // network
    lagTolerance: number;

    // animations params
    garbageEntryAnimationRate: number;
    lineClearAnimationRate: number;
}

export enum LockComboMode { INCREASES_COMBO, MAINTAINS_COMBO, DOES_NOT_COUNT };
export enum GarbageBlockingMode { ATTACK, BLOCK, BOTH };
export enum SpinBonusMode { NO_BONUS, T_SPIN_ONLY, IMMOBILE };
export enum RotationMode { NO_KICK, NEAREST, SRS };
export enum PieceGenerationMode { BAG, MEMORY, RANDOM };
export enum HoldMode { NO_HOLD, ONCE_ONLY, INFINITE };
export enum TargetingMode { EVEN, LEAST, NEXT, LEAST_ON_SCREEN };
export enum ComboMode { CONSECUTIVE_CLEARS, TIMER, NONE };

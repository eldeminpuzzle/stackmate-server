import { GarbageData, SideData } from "./gameData";
import { SpawnGarbageCommand, TileType } from "./gameIntf";
import { SideRule } from "./gameRule";
import { RandomGen } from "./randomGen";

export class GarbageGen {

    private freePosPrev: number[];
    private r: RandomGen;

    constructor(
        private sideRule: SideRule,
        private sideData: SideData,
        r: RandomGen
    ) {
        this.r = new RandomGen(r.int());
        this.freePosPrev = this.createEmptyFreePos(sideRule.width);
    }

    private createEmptyFreePos(width: number) {
        let ret = new Array(width);
        for (let j = 0; j < this.sideRule.garbageEdgeLimitation; j++) {
            ret[j] = 0;
            ret[width - j - 1] = 0;
        }
        for (let j = this.sideRule.garbageEdgeLimitation; j < width - this.sideRule.garbageEdgeLimitation; j++) {
            ret[j] = width - j - this.sideRule.garbageEdgeLimitation;
        }
        return ret;
    }

    createRow(garbageData: GarbageData): TileType[][] {
        let width = this.sideRule.width;

        let rows: TileType[][] = [];
        let firstRow = garbageData.lastRow == null;
        let stopSpawningHoles = false;
        for (let h = 0; h < garbageData.heightLeft; h++) {
            // decide clean or cheese
            let isClean = firstRow ? this.r.chance(this.sideRule.garbageCleanlinessBetween / 100)
                                : this.r.chance(this.sideRule.garbageCleanlinessWithin / 100);
            
            if (firstRow || !isClean) {
                // generate new row
                let freePosCurrent = this.createEmptyFreePos(width);
                let newRow: TileType[] = new Array(width);
                for (let j = 0; j < width; j++) {
                    newRow[j] = TileType.GARBAGE;
                }

                for (let holeData of garbageData.holes) {
                    // try to spawn disconnected prev
                    let possiblePoses: number[] = [];
                    for (let j = 0; j < width; j++) {
                        if (this.freePosPrev[j] >= holeData.width  // prev
                            && freePosCurrent[j] >= holeData.width + (j+holeData.width == (width-this.sideRule.garbageEdgeLimitation) ? 0 : 1)                  // current right
                            && (j == this.sideRule.garbageEdgeLimitation || freePosCurrent[j-1] != 0)                     // current left
                        ) {
                            possiblePoses.push(j);
                        }
                    }

                    if (possiblePoses.length > 0) {
                        // create disconnected prev
                        let pos = this.r.pick(possiblePoses);
                        for (let j = pos; j < pos + holeData.width; j++) {
                            newRow[j] = holeData.type;
                            freePosCurrent[j] = 0;
                        }
                        for (let j = pos-1; j >= 0; j--) {
                            if (freePosCurrent[j] == 0) break;
                            freePosCurrent[j] = pos - j;
                        }
                    } else {
                        // if no space for disconnected, try to spawn disconnected current
                        possiblePoses = [];
                        for (let j = 0; j < width; j++) {
                            if (freePosCurrent[j] >= holeData.width + (j+holeData.width == (width-this.sideRule.garbageEdgeLimitation) ? 0 : 1) // current right
                                && (j == this.sideRule.garbageEdgeLimitation || freePosCurrent[j-1] != 0) // current left
                            ) {
                                possiblePoses.push(j);
                            }
                        }

                        if (possiblePoses.length > 0) {
                            let pos = this.r.pick(possiblePoses);
                            for (let j = pos; j < pos + holeData.width; j++) {
                                newRow[j] = holeData.type;
                                freePosCurrent[j] = 0;
                            }
                            for (let j = pos-1; j >= 0; j--) {
                                if (freePosCurrent[j] == 0) break;
                                freePosCurrent[j] = pos - j;
                            }
                        } else {
                            // if fail to spawn disconnected at all, stop spawning (not all holes will be spawned)
                            stopSpawningHoles = true;
                        }
                    }

                    if (stopSpawningHoles) break;
                }
                
                rows.push(newRow);
                garbageData.lastRow = newRow;
                firstRow = false;
                this.freePosPrev = freePosCurrent;
            } else {
                rows.push(garbageData.lastRow);
            }
        }
        return rows;
    }
}


import { GarbageData, SideData } from "./gameData";

export class GarbageQueue {

    constructor(
        private sideData: SideData
    ) {}

    addGarbage(garbageData: GarbageData) {
        this.sideData.garbageQueue.push(garbageData);
    }

    block(power: number) {
        let blockedPower = 0;
        let queue = this.sideData.garbageQueue;
        for (let i = 0; i < queue.length; i++) {
            let garbageData = queue[i];
            if (power < garbageData.heightLeft) {
                blockedPower += power;
                queue.splice(0, i);
                garbageData.heightLeft -= Math.floor(power);
                return blockedPower;
            } else if (power == garbageData.heightLeft) {
                queue.splice(0, i + 1);
                blockedPower += power;
                return blockedPower;
            } else {
                blockedPower += garbageData.heightLeft;
                power -= garbageData.heightLeft;
            }
        }
        queue.splice(0);
        return blockedPower;
    }

    isDelayReached(garbageData: GarbageData) {
        return Date.now() - garbageData.queuedAt >= garbageData.delayTime*1000;
    }

    nextIsReady() {
        let queue = this.sideData.garbageQueue;
        return queue.length > 0 && this.isDelayReached(queue[0]);
    }

    next(limit: number, ignoreGarbageDelay: boolean): {gb: GarbageData[], carry: GarbageData} {
        let queue = this.sideData.garbageQueue;
        if (limit == -1) {
            // no limit, spawn everything
            return {gb: queue.splice(0), carry: null};
        }
        
        for (let i = 0; i < queue.length; i++) {
            if (!ignoreGarbageDelay && !this.isDelayReached(queue[i])) {
                // if garbage delay is not reached, do not spawn
                limit = 0;
            }

            if (limit > queue[i].heightLeft) {
                // limit is over the current garbage -> continue to next garbage
                limit -= queue[i].heightLeft;
            } else {
                // limit is reached -> spawn up to current garbage and cull current garbage
                let ret = queue.slice(0, i);
                ret.push({
                    holes: queue[i].holes,
                    totalHeight: queue[i].totalHeight,
                    heightLeft: limit,
                    delayTime: queue[i].delayTime,
                    queuedAt: queue[i].queuedAt,
                    lastRow: queue[i].lastRow,
                });

                queue[i].heightLeft -= limit;
                let carry = queue[i].heightLeft == 0 ? null : queue[i];
                queue.splice(0, i + (queue[i].heightLeft == 0 ? 1 : 0));
                return {gb: ret, carry};
            }
        }
        return {gb: queue.splice(0), carry: null};
    }
}

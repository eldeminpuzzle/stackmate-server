
export class Goal {
    constructor (
        private name: string,
        private target: number,
        private progress=0
    ) {

    }

    addProgress(amount: number) {
        this.progress += amount;
    }

    isComplete() {
        return this.progress >= this.target;
    }

    getFactor() {
        return Math.max(0, Math.min(1, this.progress / this.target));
    }

    getName() { return this.name; }
    getProgress() { return this.progress; }
    getTarget() { return this.target; }
}

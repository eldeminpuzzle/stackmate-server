
export interface PresetInfo {
    position: number;
    name: string;
    dsc: string;
}

// to be sent to client in lobby
export interface RoomInfo {
    id: number;
    name: string;
    presetName: string;
    serverRoom: boolean;
    host: number;
    hostIsSpectator: boolean;
    players: PlayerInfo[];
    spectators: PlayerInfo[];
    running: boolean;
    isHost: boolean;
    seatNr: number;
    maxPlayers: number; // null = follow game rule. 0 = unlimited auto join. 16 = auto join up to 16 players.

    custom: boolean;
    autoStart: number;
    autoJoin: boolean;
}

export interface PlayerInfo {
    username: string;
}

export interface RoomEnteredInfo {
    seatNr: number;
    room: RoomInfo;
    gameRuleJson: string;
}

export interface PlayerJoinedInfo {
    seatNr: number;
    player: PlayerInfo;
}

export interface AccountInfo {
    name: string;
    anonymous: boolean;
    coins: number;
}


export interface RatingData {
    rating: number;
    rd: number;
    vol: number;
    matches: number;
}

export interface RatingInfo {
    seasonPoints: number;
    ratingData: RatingData;
}

export interface RatingUpdateInfo {
    old: RatingInfo;
    new: RatingInfo;
}

export interface GameResult {
    roomId: number;
    coinReward?: number;
    ratingUpdateInfo?: RatingUpdateInfo;
    ranking: PlayerInfo[];
}


// leaderboard
export interface LeaderboardListItem {
    id: number;
    name: string;
    maxSeason: number;
}

export interface LeaderboardData {
    gameModeName: string;
    season: number;
    offset: number;
    rows: LeaderboardDataRow[];
}

export interface LeaderboardDataRow {
    username: string;
    score: number;
}
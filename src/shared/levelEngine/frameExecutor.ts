import { TileType } from "../gameIntf";
import { ClearInfo, PlayerEngine } from "../playerEngine";
import { CommandType, Frame, FrameType, GoalType, Script } from "./levelScript";

// const testLevel: Script = {
//     mainFrame: {
//         type: FrameType.LINEAR,
//         children: [{
//             type: FrameType.PARALLEL,
//             children: [{
//                 commands: [{
//                     cmd: CommandType.GOAL,
//                     goalType: GoalType.LINES,
//                     target: 2,
//                 }],
//             }, {
//                 commands: [{
//                     cmd: CommandType.GOAL,
//                     goalType: GoalType.LINES,
//                     target: 4,
//                 }],
//             }]
//         }, {
//             commands: [{
//                 cmd: CommandType.GOAL,
//                 goalType: GoalType.LINES,
//                 target: 7,
//             }]
//         }, {
//             commands: [{
//                 cmd: CommandType.FINISH,
//                 goalType: GoalType.LINES,
//             }]
//         }]
//     }
// };

const testLevel: Script = {
    mainFrame: {
        type: FrameType.LINEAR,
        children: [{
            type: FrameType.LINEAR,
            repeat: 10,
            children: [{
                commands: [{
                    cmd: CommandType.WAIT,
                    seconds: 6,
                }]
            }, {
                commands: [{
                    cmd: CommandType.SPAWN_GARBAGE,
                    height: 20,
                    delay: 2,
                }, {
                    cmd: CommandType.WAIT,
                    seconds: 12,
                }]
            }]
        }, {
            commands: [{
                cmd: CommandType.FINISH,
                goalType: GoalType.LINES,
            }]
        }]
    }
};

export class FrameExecutor {
    private finished = false;

    private currentFrame = -1; // TODO: this is linear frane only
    private repeatCount = 0; // TODO: this is linear frame only
    private parallelFinishedCount = 0; // TODO: this is parallel frame only

    private disposeList: any[] = [];
    private executorId: number;

    constructor(
        private root: RootFrameExecutor,
        private parent: FrameExecutor,
        private frame: Frame,
        private onFinish: () => void = null,
    ) {}

    public start() {
        this.root.playerEngine.listenersOnRoundEnded.add(() => this.dispose());
        this.executeCommands();

        if (this.frame.type == FrameType.LINEAR) {
            this.nextFrame();
        } else if (this.frame.type == FrameType.PARALLEL) {
            for (let childFrame of this.frame.children) {
                let child = new FrameExecutor(this.root, this, childFrame, this.onFrameFinished.bind(this));
                child.start();
            }
        }
    }

    // TODO: this is parallel only
    private onFrameFinished() {
        this.parallelFinishedCount++;
        if (this.parallelFinishedCount == this.frame.children.length) {
            this.finishFrame();
        }
    }

    // TODO: this is linear only
    private nextFrame() {
        // update currentFrame, taking repeat into account
        this.currentFrame++;
        if (this.currentFrame >= this.frame.children.length && this.frame.repeat != null) {
            this.repeatCount++;
            if (this.frame.repeat == 0 || this.frame.repeat > this.repeatCount) {
                this.currentFrame = 0;
            }
            console.log('REPEAT ', this.repeatCount);
        }

        // next frame or finish if last frame
        if (this.currentFrame < this.frame.children.length) {
            let child = new FrameExecutor(this.root, this, this.frame.children[this.currentFrame], this.nextFrame.bind(this));
            child.start();
        } else {
            this.finishFrame();
        }
    }

    private executeCommands() {
        // TODO: refactor command type
        if (this.frame.commands) {
            this.frame.commands.forEach(c => {
                if (c.cmd == CommandType.GOAL) {
                    if (c.goalType == GoalType.LINES) {
                        let goal = this.root.playerEngine.addGoal("Lines", c.target);
                        let listener = (clearInfo: ClearInfo) => {
                            goal.addProgress(clearInfo.linesCleared.length);
                            if (goal.isComplete()) {
                                this.finishFrame();
                            }
                        };
                        this.root.playerEngine.listenersOnLinesCleared.add(listener);
                        this.disposeList.push(() => this.root.playerEngine.listenersOnLinesCleared.delete(listener));
                    } else {
                        throw new Error("Required GoalType");
                    }
                } else if (c.cmd == CommandType.WAIT) {
                    // TODO: ACK!!!
                    this.withAck(() => {
                        let timeout = setTimeout(() => {
                            this.finishFrameWithAck();
                        }, c.seconds * 1000);
                        this.disposeList.push(() => clearTimeout(timeout));
                    });
                } else if (c.cmd == CommandType.SPAWN_GARBAGE) {
                    this.root.playerEngine.addGarbage(
                        [{type: TileType.BOMB, width: 1}], c.height, c.delay);
                } else if (c.cmd == CommandType.FINISH) {
                    this.root.playerEngine.lockOut(); // TODO: win
                } else {
                    throw new Error("Unsupported command");
                }
            });
        }
    }

    withAck(localCallback: () => void) {
        this.executorId = this.root.nextExecutorId(this);
        if (this.root.playerEngine.isLocal) {
            localCallback();
        }
    }

    finishFrameWithAck() {
        this.root.pushAckExecutorId(this.executorId);
        this.finishFrame();
    }

    finishDueToAck() {
        this.finishFrame();
    }

    private finishFrame() {
        if (!this.finished) {
            this.finished = true;

            this.dispose();
            this.onFinish();
        }
    }

    public dispose() {
        this.disposeList.forEach(d => d());
        this.disposeList = [];
    }
}

export class RootFrameExecutor {
    private executorMap: Map<number, FrameExecutor> = new Map();
    private _nextExecutorId = 0;
    
    // private rootExecutor: FrameExecutor;

    private ackExecutorsIds: number[] = [];

    public onAckPushed: (() => void); // TODO: refactor. executorMap is no longer needed if every ACK triggers sub-move transmission.

    constructor(
        public playerEngine: PlayerEngine
    ) {
        // this.rootExecutor = new FrameExecutor(this, null, testLevel.mainFrame);
    }

    public pushAckExecutorId(executorId: number) {
        this.ackExecutorsIds.push(executorId);
        this.onAckPushed();
    }

    public pullAckExecutorIds() {
        let ret = this.ackExecutorsIds;
        this.ackExecutorsIds = [];
        return ret;
    }

    public nextExecutorId(frameExecutor: FrameExecutor) {
        let id = this._nextExecutorId;
        this._nextExecutorId++;

        this.executorMap.set(id, frameExecutor)
        return id;
    }

    /** To be called when ack has been received. */
    public receiveAck(executorIds: number[]) {
        for (let id of executorIds) {
            this.executorMap.get(id).finishDueToAck();
            this.executorMap.delete(id);
        }
    }
    


    public start() {
        // this.rootExecutor.start();
    }

    public dispose() {
        // this.rootExecutor.dispose();
    }
}

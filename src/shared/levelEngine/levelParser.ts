
// T-156: g=goalLines(40)---finish(min, time)

import { Script } from "./levelScript";
import { Token, TokenType } from "./levelTokenizer";

const DIGITS = '1234567890';
const SYMBOLS = '();';
const IDENT_END = SYMBOLS + ' ';

export class LevelParser {
    private script: string;
    private pos = 0;

    constructor(script: string) {
        this.script =  this.removeWhiteSpaces(this.removeComments(script));
    }

    private nextToken(): Token {
        while (/\s/.test(this.getChar())) {
            this.pos++;
        }

        let char = this.getChar();
        if (char == null) {
            return null;
        } else if (SYMBOLS.indexOf(char) != -1) {
            this.pos++;
            return { type: TokenType.SYMBOL, value: char };
        } else if (this.isDigit()) {
            let str = '';
            str = this.nextNumber();
            if (this.getChar() == '.') {
                str += '.' + this.nextNumber();
            }
            return { type: TokenType.NUMBER, value: parseFloat(str) };
        } else {
            let str = '';
            while (IDENT_END.indexOf(this.getChar()) == -1) {
                str += this.getChar();
                this.pos++;
            }

        }
    }

    private nextNumber() {
        let str = '';
        while (this.isDigit()) {
            str += this.script.charAt(this.pos);
            this.pos++;
        }
        if (str.length == 0) {
            throw new Error('Digit expected!');
        }
        return str;
    }

    private getChar() {
        return this.pos < this.script.length ? this.script.charAt(this.pos) : null;
    }

    private isDigit() {
        return DIGITS.indexOf(this.script.charAt(this.pos)) != -1;
    }

    private removeComments(script: string): string {
        return script;
    }

    private removeWhiteSpaces(script: string): string {
        return script;
    }
}

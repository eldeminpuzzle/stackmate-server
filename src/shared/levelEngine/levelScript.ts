
// T-156: g=goalLines(40)---finish(min, time)

export enum FrameType { LINEAR, PARALLEL };
export enum ExpressionType { COMMAND, LITERAL_NUMBER, LITERAL_ENUM };
export enum CommandType { GOAL, FINISH, WAIT, SPAWN_GARBAGE };
export enum GoalType { LINES };

export interface Script {
    mainFrame: Frame;
}

export interface Frame {
    type?: FrameType;
    repeat?: number; // 0 = infinite
    children?: Frame[];

    commands?: any[];
}

import { prng } from "seedrandom";
import { Piece } from "../shared/gameData";
import { Tile } from "../shared/gameData";
import { RandomGen } from "./randomGen";


interface PieceData {
    name: string;
    matSize: number;
    sparse: number[][];
    color?: number;
}

export interface PieceList {
    size: number;
    pieceId: number; // if null, then spawn all pieces
    multiplier: number; // if decimal, some pieces will be missing
}

export const PIECES: PieceData[][] = [
    [// 1
        { name: '.', matSize: 1, sparse: [[0, 0]] }
    ], [ // 2
        { name: ':', matSize: 2, sparse: [[1, 0], [1, 1]] }
    ], [ //3
        { name: 'I', matSize: 3, sparse: [[1, 0], [1, 1], [1, 2]] },
        { name: 'L', matSize: 2, sparse: [[0, 1], [1, 0], [1, 1]] }
    ], [ // 4
        { name: 'I', matSize: 4, color: 4, sparse: [[1, 0], [1, 1], [1, 2], [1, 3]] },
        { name: 'L', matSize: 3, color: 1, sparse: [[0, 2], [1, 0], [1, 1], [1, 2]] },
        { name: 'J', matSize: 3, color: 5, sparse: [[0, 0], [1, 0], [1, 1], [1, 2]] },
        { name: 'S', matSize: 3, color: 3, sparse: [[0, 1], [0, 2], [1, 0], [1, 1]] },
        { name: 'Z', matSize: 3, color: 0, sparse: [[0, 0], [0, 1], [1, 1], [1, 2]] },
        { name: 'T', matSize: 3, color: 6, sparse: [[0, 1], [1, 0], [1, 1], [1, 2]] },
        { name: 'O', matSize: 2, color: 2, sparse: [[0, 0], [0, 1], [1, 0], [1, 1]] },
    ], [ // 5
        { name: 'I', matSize: 5, sparse: [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'L', matSize: 4, sparse: [[1, 3], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'J', matSize: 4, sparse: [[1, 0], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'H', matSize: 4, sparse: [[1, 2], [1, 3], [2, 0], [2, 1], [2, 2]] },
        { name: 'N', matSize: 4, sparse: [[1, 0], [1, 1], [2, 1], [2, 2], [2, 3]] },
        { name: 'Y', matSize: 4, sparse: [[1, 2], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'R', matSize: 4, sparse: [[1, 1], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'E', matSize: 3, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [2, 0]] },
        { name: 'F', matSize: 3, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [2, 2]] },
        { name: 'P', matSize: 3, sparse: [[0, 0], [0, 1], [1, 0], [1, 1], [1, 2]] },
        { name: 'Q', matSize: 3, sparse: [[0, 1], [0, 2], [1, 0], [1, 1], [1, 2]] },
        { name: 'S', matSize: 3, sparse: [[0, 1], [0, 2], [1, 1], [2, 0], [2, 1]] },
        { name: 'Z', matSize: 3, sparse: [[0, 0], [0, 1], [1, 1], [2, 1], [2, 2]] },
        { name: 'T', matSize: 3, sparse: [[0, 1], [1, 1], [2, 0], [2, 1], [2, 2]] },
        { name: 'C', matSize: 3, sparse: [[0, 0], [0, 2], [1, 0], [1, 1], [1, 2]] },
        { name: 'V', matSize: 3, sparse: [[0, 2], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'W', matSize: 3, sparse: [[0, 2], [1, 1], [1, 2], [2, 0], [2, 1]] },
        { name: 'X', matSize: 3, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [2, 1]] },
    ], [ // 6
        { name: 'I', matSize: 6, sparse: [[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5]] },
        { name: 'Li', matSize: 5, sparse: [[1, 4], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'Ji', matSize: 5, sparse: [[1, 0], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'Ti', matSize: 5, sparse: [[1, 2], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'Lo', matSize: 4, sparse: [[0, 3], [1, 3], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Jo', matSize: 4, sparse: [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'P', matSize: 4, sparse: [[1, 0], [1, 1], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Q', matSize: 4, sparse: [[1, 2], [1, 3], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Tr', matSize: 4, sparse: [[0, 2], [1, 2], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Tl', matSize: 4, sparse: [[0, 1], [1, 1], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'D', matSize: 4, sparse: [[1, 1], [1, 2], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'C', matSize: 4, sparse: [[1, 0], [1, 3], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'F', matSize: 4, sparse: [[1, 0], [2, 0], [2, 1], [2, 2], [2, 3], [3, 1]] },
        { name: 'E', matSize: 4, sparse: [[1, 3], [2, 0], [2, 1], [2, 2], [2, 3], [3, 2]] },
        { name: 'So', matSize: 4, sparse: [[0, 0], [1, 0], [1, 1], [1, 2], [1, 3], [2, 3]] },
        { name: 'Zo', matSize: 4, sparse: [[0, 3], [1, 0], [1, 1], [1, 2], [1, 3], [2, 0]] },
        { name: 'Xr', matSize: 4, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [1, 3], [2, 2]] },
        { name: 'Xl', matSize: 4, sparse: [[0, 2], [1, 0], [1, 1], [1, 2], [1, 3], [2, 1]] },
        { name: '0', matSize: 3, sparse: [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2]] },
        { name: 'Wr', matSize: 4, sparse: [[0, 2], [0, 3], [1, 1], [1, 2], [2, 0], [2, 1]] },
        { name: 'Wl', matSize: 4, sparse: [[0, 0], [0, 1], [1, 1], [1, 2], [2, 2], [2, 3]] },
        { name: 'Ur', matSize: 3, sparse: [[0, 2], [1, 0], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'Ul', matSize: 3, sparse: [[0, 0], [1, 0], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'S', matSize: 4, sparse: [[0, 2], [0, 3], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'Z', matSize: 4, sparse: [[0, 0], [0, 1], [1, 1], [2, 1], [2, 2], [2, 3]] },
        { name: 'Mi', matSize: 5, sparse: [[1, 3], [1, 4], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Ni', matSize: 5, sparse: [[1, 0], [1, 1], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'Si', matSize: 5, sparse: [[1, 2], [1, 3], [1, 4], [2, 0], [2, 1], [2, 2]] },
        { name: 'Zi', matSize: 5, sparse: [[1, 0], [1, 1], [1, 2], [2, 2], [2, 3], [2, 4]] },
        { name: 'Rr', matSize: 3, sparse: [[0, 1], [1, 0], [1, 1], [2, 0], [2, 1], [2, 2]] },
        { name: 'Rl', matSize: 3, sparse: [[0, 1], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'Pi', matSize: 3, sparse: [[0, 0], [1, 0], [1, 1], [1, 2], [2, 1], [2, 2]] },
        { name: 'Qi', matSize: 3, sparse: [[0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1]] },
        { name: 'V', matSize: 3, sparse: [[0, 0], [0, 2], [1, 0], [1, 1], [1, 2], [2, 1]] },
        { name: '4r', matSize: 4, sparse: [[0, 2], [1, 0], [1, 1], [1, 2], [2, 2], [2, 3]] },
        { name: '4l', matSize: 4, sparse: [[0, 1], [1, 1], [1, 2], [1, 3], [2, 0], [2, 1]] },
        { name: '?', matSize: 4, sparse: [[1, 0], [1, 1], [1, 3], [2, 1], [2, 2], [2, 3]] },
        { name: '5', matSize: 4, sparse: [[1, 0], [1, 2], [1, 3], [2, 0], [2, 1], [2, 2]] },
        { name: '\\', matSize: 4, sparse: [[0, 3], [1, 1], [1, 2], [1, 3], [2, 0], [2, 1]] },
        { name: '/', matSize: 4, sparse: [[0, 0], [1, 0], [1, 1], [1, 2], [2, 2], [2, 3]] },
        { name: '1r', matSize: 5, sparse: [[1, 1], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: '1l', matSize: 5, sparse: [[1, 3], [2, 0], [2, 1], [2, 2], [2, 3], [2, 4]] },
        { name: 'Fi', matSize: 4, sparse: [[1, 0], [1, 2], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'Ei', matSize: 4, sparse: [[1, 1], [1, 3], [2, 0], [2, 1], [2, 2], [2, 3]] },
        { name: 'To', matSize: 5, sparse: [[0, 2], [1, 2], [2, 2], [3, 1], [3, 2], [3, 3]] },
        { name: 'Fr', matSize: 4, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [1, 3], [2, 3]] },
        { name: 'Fl', matSize: 4, sparse: [[0, 2], [1, 0], [1, 1], [1, 2], [1, 3], [2, 0]] },
        { name: 'Xi', matSize: 5, sparse: [[0, 2], [1, 2], [2, 1], [2, 2], [2, 3], [3, 2]] },
        { name: 'A', matSize: 3, sparse: [[0, 2], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]] },
        { name: 'St', matSize: 4, sparse: [[0, 2], [1, 1], [1, 2], [1, 3], [2, 0], [2, 1]] },
        { name: 'Zt', matSize: 4, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [2, 2], [2, 3]] },
        { name: 'No', matSize: 4, sparse: [[1, 0], [1, 1], [1, 2], [2, 1], [2, 2], [2, 3]] },
        { name: 'Mo', matSize: 4, sparse: [[1, 1], [1, 2], [1, 3], [2, 0], [2, 1], [2, 2]] },
        { name: 'K', matSize: 3, sparse: [[0, 1], [1, 0], [1, 1], [1, 2], [2, 1], [2, 2]] },
        { name: 'h', matSize: 3, sparse: [[0, 0], [0, 2], [1, 0], [1, 1], [1, 2], [2, 2]] },
        { name: 'μ', matSize: 3, sparse: [[0, 0], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0]] },
        { name: 'Po', matSize: 4, sparse: [[0, 1], [1, 0], [1, 1], [2, 1], [2, 2], [2, 3]] },
        { name: 'Qo', matSize: 4, sparse: [[0, 2], [1, 2], [1, 3], [2, 0], [2, 1], [2, 2]] },
    ]
];

export function createPiece(size: number, pieceId: number): Piece {
    let p = PIECES[size - 1][pieceId];

    let mat: Tile[][] = new Array();
    for (let i = 0; i < p.matSize; i++) {
        mat.push(new Array(p.matSize));
    }

    for (let t of p.sparse) {
        mat[t[0]][t[1]] = {
            colorId: p.color != null ? p.color : (pieceId % 7),
            isGarbage: false,
        };
    }

    return {
        tiles: mat,
        size,
        id: pieceId
    };
}

export interface PieceGen {
    next(): Piece;
    nextId(): number[]; // like next() but returns only the piece size + id
}

// export class MemoryPieceGen implements PieceGen {
//     bag: any[] = [];
//     mem: any[] = [];
//     nextMem = 0;

//     constructor(
//         private m: number
//     ) {
//         this.reset();
//     }

// 	next(): Piece {
//         return createPiece(4, this.nextId());
//     }

//     nextId(): number {
//         let r = Math.floor(Math.random() * this.bag.length);
//         let id = this.bag[r];
//         this.bag[r] = this.mem[this.nextMem];
//         this.mem[this.nextMem] = id;
//         this.nextMem = (this.nextMem + 1) % this.mem.length;
//         return id;
//     }

//     reset() {
//         this.bag = [0, 1, 2, 3, 4, 5, 6];
//         shuffle(this.bag);
//         this.mem = this.bag.splice(6 - this.m);
//     }
// }

// export class DefaultPieceGen implements PieceGen {
// 	next(): Piece {
//         return createPiece(4, this.nextId());
//     }

// 	nextId(): number {
// 		return Math.floor(Math.random() * 7);
//     }
// }

function getBag(pieceList: PieceList[]) {
    let bag: number[][] = [];
    // refill bag
    for (let e of pieceList) {
        for (let m = 0; m < e.multiplier; m++) {
            if (e.pieceId == null) {
                for (let i = 0; i < PIECES[e.size - 1].length; i++) {
                    bag.push([e.size, i]);
                }
            } else {
                bag.push([e.size, e.pieceId]);
            }
        }
    }
    return bag;
}

export class RandomPieceGen implements PieceGen {
    private bag: number[][] = []; // [pos][size,pieceId]
    ;

    constructor(
        private r: RandomGen,
        pieceList: PieceList[]
    ) {
        this.bag = getBag(pieceList);
    }

    next(): Piece {
        let [size, id] = this.nextId();
        return createPiece(size, id);
    }

    nextId(): number[] {
        return this.r.pick(this.bag);
    }

    reset() {}
}

export class MemoryPieceGen implements PieceGen {
    private bag: number[][] = []; // [pos][size,pieceId]
    private mem: number[][] = [];
    private nextMemId = 0;

    constructor(
        private r: RandomGen,
        pieceList: PieceList[],
        memSize: number
    ) {
        this.bag = getBag(pieceList);
        if (memSize >= this.bag.length) {
            console.log('INVALID MEM SIZE piece gen');
            memSize = this.bag.length - 1;
        }
        this.mem = this.bag.splice(this.bag.length - memSize);
    }

    next(): Piece {
        let [size, id] = this.nextId();
        return createPiece(size, id);
    }

    nextId(): number[] {
        let r = this.r.int(this.bag.length);
        let ret = this.bag[r];
        this.bag[r] = this.mem[this.nextMemId];
        this.mem[this.nextMemId] = ret;
        this.nextMemId = (this.nextMemId + 1) % this.mem.length;
        return ret;
    }

    reset() {
        this.mem = [];
    }
}

export class BagPieceGen implements PieceGen {
    private bag: number[][] = []; // [pos][size,pieceId]

    constructor(
        private r: RandomGen,
        private pieceList: PieceList[]
    ) {}

    next(): Piece {
        let [size, id] = this.nextId();
        return createPiece(size, id);
    }

    nextId(): number[] {
        if (this.bag.length == 0) {
            // refill bag
            for (let e of this.pieceList) {
                for (let m = 0; m < e.multiplier; m++) {
                    if (e.pieceId == null) {
                        for (let i = 0; i < PIECES[e.size - 1].length; i++) {
                            this.bag.push([e.size, i]);
                        }
                    } else {
                        this.bag.push([e.size, e.pieceId]);
                    }
                }
            }
            this.shuffle(this.bag);
        }

        return this.bag.pop();
    }

    private connectivity(bMat: number[][]) {
        return [
            this.isTile(bMat, -1, 0),
            this.isTile(bMat, 0, -1),
            this.isTile(bMat, 0, 1),
            this.isTile(bMat, 1, 0),
        ];
    }

    private isTile(bMat: number[][], y: number, x: number): boolean {
        return y >= 0
            && x >= 0
            && y <= bMat.length
            && x <= bMat[y].length
            && bMat[y][x] == 1;
    }

    reset() {
        this.bag = [];
    }

    
    shuffle(array: any[]) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = this.r.int(currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}
